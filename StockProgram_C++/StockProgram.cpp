#include <cstdlib>
#include <iostream>
#include "StockItem.h"
#include "Sales.h"

using namespace std;

int main(int argc, char** argv) {

// QUESTION 1 ******************************************************************
    
    Inventory* I = new Inventory();
    I->loadInventory();
    I->sort();
    
    cout << I << endl;    
    cout << endl;
    
// QUESTION 2 ******************************************************************

    SaleList* S = new SaleList();
    S->processTransactions(I);
    
    cout << endl;
    S->findSalesVolumePrice();
    S->findSalesVolumeQuantity();
    cout << endl;
    
// QUESTION 3 ******************************************************************

    cout << "NUMBER OF NPN TRANSISTORS: " << I->countTransistors("NPN") << endl;
    cout << endl;
    
// QUESTION 4 ******************************************************************    
   
    I->findTotalResistance();
    cout << endl;
    
    return 0;
}