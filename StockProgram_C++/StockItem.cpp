#include <cstdlib>
#include <iostream>
#include <string.h>
#include "StockItem.h"

// STOCKITEM IS AN ABSTRACT BASE CLASS, AND SO HAS NO CONSTRUCTOR.

// THIS FUNCTION RETURNS THE COMPONENT TYPE OF THE INVOKING STOCK ITEM OBJECT.
char* StockItem::getComponentType(){
    return componentType;
}


// THIS FUNCTION RETURNS THE STOCK CODE OF THE INVOKING STOCK ITEM OBJECT.
char* StockItem::getStockCode(){
    return stockCode;
}


// THIS FUNCTION RETURNS THE STOCK QUANTITY OF THE INVOKING STOCK ITEM OBJECT.
int StockItem::getStockQuantity(){
    return stockQuantity;
}


// THIS FUNCTION RETURNS THE UNIT PRICE OF THE INVOKING STOCK ITEM OBJECT.
int StockItem::getUnitPrice(){
    return unitPrice;
}


// THIS FUNCTION ASSIGNS THE COMPONENT TYPE OF THE INVOKING STOCK ITEM OBJECT
// WITH THE PASSED CHARACTER ARRAY.
void StockItem::setComponentType(char* S){

    char string[15];
    strcpy(string,S);
    
    for(int i = 0; i < strlen(string); i++){
        string[i] = tolower(string[i]);
    }
// THE PASSED CHARACTER ARRAY IS FIRST COPIED AND CONVERTED TO LOWERCASE.    

    char* S1 = "resistor";
    char* S2 = "capacitor";
    char* S3 = "transistor";
    char* S4 = "diode";
    char* S5 = "ic";

    if(strcmp(S,S1) != 0 && strcmp(S,S2) != 0 && strcmp(S,S3) != 0 && strcmp(S,S4) != 0 && strcmp(S,S5) != 0){
        cout << "PLEASE SELECT A COMPONENT TYPE FROM ONE OF THE FOLLOWING: resistor, diode, capacitor, transistor, IC" << endl;
// IF THE PASSED CHARACTER ARRAY DOES NOT MATCH ANY OF THE FIVE ABOVE, THEN THE
// ERROR MESSAGE IS PRINTED AND NOTHING OCCURS.        
    }
    else{
        strcpy(componentType,S);
// OTHERWISE, THE PASSED CHARACTER ARRAY IS COPIED INTO THE FIELD VARIABLE.        
    }
}


// THIS FUNCTION ASSIGNS THE STOCK CODE OF THE INVOKING OBJECT TO THE PASSED 
// CHARACTER ARRAY, S.
void StockItem::setStockCode(char* S){
    strcpy(stockCode,S);    
}


// THIS FUNCTION ASSIGNS THE STOCK QUANTITY OF THE INVOKING OBJECT TO THE PASSED
// INTEGER, I.
void StockItem::setStockQuantity(int I){
    stockQuantity = I;
}


// THIS FUNCTION ASSIGNS THE UNIT PRICE OF THE INVOKING OBJECT TO THE PASSED
// INTEGER, I.
void StockItem::setUnitPrice(int I){
    unitPrice = I;
}




// THE DIODE CLASS CONSTRUCTOR
Diode::Diode(char* code, int quantity, int price){

    strcpy(componentType,"diode");
    strcpy(stockCode,code);
    stockQuantity = quantity;
    unitPrice = price;
}  


// THIS FUNCTION OVERRIDDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. BECAUSE
// VIRTUAL FUNCTIONS MUST BE OVERRIDDEN, AND A DIODE HAS NO ADDITIONAL INFORMATION,
// AN ERROR MESSAGE IS PRINTED AND NULL IS RETURNED.
char* Diode::getComponentInfo(){
    cout << "NOT AVAILABLE FOR DIODE CLASS OBJECTS" << endl;    
    return NULL;
}


// THIS FUNCTION OVERRIDDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. BECAUSE
// VIRTUAL FUNCTIONS MUST BE OVERRIDDEN, AND A DIODE HAS NO ADDITIONAL INFORMATION,
// AN ERROR MESSAGE IS PRINTED.
void Diode::setComponentInfo(char* S){
    cout << "NOT AVAILABLE FOR DIODE CLASS OBJECTS" << endl;
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING DIODE OBJECT.
void Diode::print(){
    cout << "ITEM NO: " << left << setw(15) << this->getStockCode() 
         << "COMPONENT TYPE: " << left << setw(15) << this->getComponentType()
         << "QUANTITY: " << left << setw(15) << this->getStockQuantity()
         << "UNIT PRICE: " << left << setw(15) << this->getUnitPrice();

// THIS IS THE FUNCTION THAT IS CALLED WHEN A DIODE CLASS OBJECT IS PASSED INTO
// A COUT << METHOD CALL.    
}




// THE RESISTOR CLASS CONSTRUCTOR
Resistor::Resistor(char* code, int quantity, int price, char* res){

    strcpy(componentType,"resistor");
    strcpy(stockCode,code);
    stockQuantity = quantity;
    unitPrice = price;
    strcpy(componentInfo,res);
}


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT RETURNS
// THE COMPONENT INFORMATION (RESISTANCE).
char* Resistor::getComponentInfo(){
    return componentInfo;
}


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT ASSIGNS
// THE COMPONENT INFORMATION TO THE PASSED CHARACTER ARRAY, S.
void Resistor::setComponentInfo(char* S){
    strcpy(componentInfo,S);
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING RESISTOR OBJECT.
void Resistor::print(){
    
    cout << "ITEM NO: " << left << setw(15) << this->getStockCode()
         << "COMPONENT TYPE: " << left << setw(15) << this->getComponentType()
         << "QUANTITY: " << left << setw(15) << this->getStockQuantity()
         << "UNIT PRICE: " << left << setw(15) << this->getUnitPrice()
         << "RESISTANCE: " << left << setw(15) << this->getComponentInfo();
    
// THIS FUNCTION IS CALLED WHEN A RESISTOR CLASS OBJECT IS PASSED THROUGH A 
// COUT << FUNCTION CALL.    
}




// THE CAPACITOR CLASS CONSTRUCTOR
Capacitor::Capacitor(char* code, int quantity, int price, char* cap){
    
    strcpy(componentType,"capacitor");
    strcpy(stockCode,code);
    stockQuantity = quantity;
    unitPrice = price;
    strcpy(componentInfo,cap);
}   


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT RETURNS
// THE COMPONENT INFORMATION (CAPACITANCE).
char* Capacitor::getComponentInfo(){
    return componentInfo;
}


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT ASSIGNS
// THE COMPONENT INFORMATION TO THE PASSED CHARACTER ARRAY, S.
void Capacitor::setComponentInfo(char* S){
    strcpy(componentInfo,S);
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING CAPACITOR OBJECT.
void Capacitor::print(){
    cout << "ITEM NO: " << left << setw(15) << this->getStockCode() 
         << "COMPONENT TYPE: " << left << setw(15) << this->getComponentType()
         << "QUANTITY: " << left << setw(15) << this->getStockQuantity()
         << "UNIT PRICE: " << left << setw(15) << this->getUnitPrice()
         << "CAPACITANCE: " << left << setw(15) << this->getComponentInfo();
    
// THIS FUNCTION IS CALLED WHEN A CAPACITOR CLASS OBJECT IS PASSED THROUGH A 
// COUT << FUNCTION CALL.      
}




// THE TRANSISTOR CLASS CONSTRUCTOR
Transistor::Transistor(char* code, int quantity, int price, char* tType){
    
    char string[5];
    strcpy(string,tType);
    
    for(int i = 0; i < strlen(string); i++){
        string[i] = tolower(string[i]);
    }

    char* str1 = "npn";
    char* str2 = "pnp";
    char* str3 = "fet";

    if(strcmp(string,str1) != 0 && strcmp(string,str2) != 0 && strcmp(string,str3) != 0){
        cout << "PLEASE ENSURE THAT THE TRANSISTOR TYPE IS ONE OF THE FOLLOWING: NPN, FET, PNP" << endl;
    }
    else{            
        strcpy(componentType,"transistor");
        strcpy(stockCode,code);
        stockQuantity = quantity;
        unitPrice = price;
        strcpy(componentInfo,tType);
    }
}   


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT RETURNS
// THE COMPONENT INFORMATION (TRANSISTOR TYPE).
char* Transistor::getComponentInfo(){
    return componentInfo;
}


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT ASSIGNS
// THE COMPONENT INFORMATION TO THE PASSED CHARACTER ARRAY, S.
void Transistor::setComponentInfo(char* S){

    char string[5];
    strcpy(string,S);
    
    for(int i = 0; i < strlen(string); i++){
        string[i] = tolower(string[i]);
    }

    char* str1 = "npn";
    char* str2 = "pnp";
    char* str3 = "fet";

    if(strcmp(string,str1) != 0 && strcmp(string,str2) != 0 && strcmp(string,str3) != 0){
        cout << "PLEASE ENSURE THAT THE TRANSISTOR TYPE IS ONE OF THE FOLLOWING: NPN, FET, PNP" << endl;
    }
    else{    
        strcpy(componentInfo,S);
    }
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING RESISTOR OBJECT.
void Transistor::print(){
    cout << "ITEM NO: " << left << setw(15) << this->getStockCode() 
         << "COMPONENT TYPE: " << left << setw(15) << this->getComponentType()
         << "QUANTITY: " << left << setw(15) << this->getStockQuantity()
         << "UNIT PRICE: " << left << setw(15) << this->getUnitPrice()
         << "TRANSISTOR TYPE: " << left << setw(15) << this->getComponentInfo(); 
    
// THIS FUNCTION IS CALLED WHEN A TRANSISTOR CLASS OBJECT IS PASSED THROUGH A 
// COUT << FUNCTION CALL.      
}




// THE IC CLASS CONSTRUCTOR
IC::IC(char* code, int quantity, int price, char* desc){

    strcpy(componentType,"ic");
    strcpy(stockCode,code);
    stockQuantity = quantity;
    unitPrice = price;
    strcpy(componentInfo,desc);
}    


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT RETURNS
// THE COMPONENT INFORMATION (IC DESCRIPTION).
char* IC::getComponentInfo(){
    return componentInfo;
}


// THIS FUNCTION OVERRIDES THE PURE VIRTUAL FUNCTION IN STOCKITEM. IT ASSIGNS
// THE COMPONENT INFORMATION TO THE PASSED CHARACTER ARRAY, S.
void IC::setComponentInfo(char* S){
    strcpy(componentInfo,S);
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING IC OBJECT.
void IC::print(){
    cout << "ITEM NO: " << left << setw(15) << this->getStockCode() 
         << "COMPONENT TYPE: " << left << setw(15) << this->getComponentType()
         << "QUANTITY: " << left << setw(15) << this->getStockQuantity()
         << "UNIT PRICE: " << left << setw(15) << this->getUnitPrice()
         << "IC DESCRIPTION: " << left << setw(15) << this->getComponentInfo();

// THIS FUNCTION IS CALLED WHEN A IC CLASS OBJECT IS PASSED THROUGH A 
// COUT << FUNCTION CALL.      
}




// THE STOCKNODE CLASS CONSTRUCTOR
StockNode::StockNode(StockItem* S){
    element = S;
}


// THIS FUNCTION RETURNS THE STOCKNODE POINTED TO BY THE INVOKING OBJECT'S
// NEXT POINTER.
StockNode* StockNode::getNext(){
    return next;
}


// THIS FUNCTION RETURNS THE STOCK ITEM ASSOCIATED WITH THE INVOKING OBJECT
StockItem* StockNode::getElement(){
    return element;
}


// THIS FUNCTION ASSIGNS THE INVOKING STOCKNODE'S NEXT POINTER TO THE PASSED 
// STOCKNODE.
void StockNode::setNext(StockNode* N){
    next = N;
}


// THIS FUNCTION ASSIGNS THE ELEMENT OF THE INVOKING STOCKNODE TO THE PASSED
// STOCKITEM.
void StockNode::setElement(StockItem* S){
    element = S;
}


// THIS FUNCTION COMPARES THE INVOKING STOCKNODE WITH THE PASSED STOCKNODE. AN
// INTEGER IS RETURNED REPRESENTING THE RELATIONSHIP BETWEEN THE TWO: 1 IF THE
// INVOKING STOCKNODE IS LARGER THAN THE PASSED STOCKNODE, -1 IF THE INVOKING 
// STOCKNODE IS SMALLER THAN THE PASSED STOCKNODE, AND 0 IF THE TWO ARE EQUAL.
int StockNode::compare(StockNode* N){

// STOCK ITEMS ARE COMPARED BY UNIT PRICE.
    
    StockItem* S1 = this->getElement();
    StockItem* S2 = N->getElement();
    
    if(S1->getUnitPrice() > S2->getUnitPrice()){
        return 1;
    }
    else if(S1->getUnitPrice() < S2->getUnitPrice()){
        return -1;
    }
    else{
        return 0;
    }   
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING STOCKNODE.
void StockNode::print(){
    cout << this->getElement();
// THIS FUNCTION IS CALLED WHEN A STOCKNODE CLASS OBJECT IS PASSED THROUGH A 
// COUT << FUNCTION CALL. THIS, IN TURN, CALLS THE PRINT FUNCTION OF THE ELEMENT.      
}




// THE STOCKLIST CLASS CONSTRUCTOR
StockList::StockList(){
    head = NULL;
    tail = NULL;
    length = 0;
}


// THIS FUNCTION RETURNS THE HEAD OF THE INVOKING STOCKLIST.
StockNode* StockList::getHead(){
    return head;
}


// THIS FUNCTION RETURNS THE TAIL OF THE INVOKING STOCKLIST.
StockNode* StockList::getTail(){
    return tail;
}


// THIS FUNCTION RETURNS AN INTEGER DENOTING THE NUMBER OF ITEMS WITHIN THE 
// INVOKING STOCKLIST.
int StockList::getLength(){
    return length;
}


// THIS FUNCTION ADDS THE PASSED STOCK ITEM TO THE END OF THE INVOKING LIST.
void StockList::add(StockItem* S){
    
    StockNode* NP = new StockNode(S);
// A NEW STOCKNODE IS DYNAMICALLY CREATED.    
    
    if(length == 0){
        head = NP;
        tail = NP;        
        length++;
// IF THE LIST IS EMPTY, THE NEW NODE IS ASSIGNED AS BOTH THE LIST HEAD AND TAIL.        
    }
    else{
        tail->setNext(NP);
        tail = NP;  
        length++;
// OTHERWISE, THE NEW NODE IS ADDED TO THE END OF THE LIST.    
    }
}


// THIS FUNCTION ADDS THE PASSED STOCK ITEM TO THE HEAD OF THE INVOKING LIST.
void StockList::insert(StockItem* S){
    
    StockNode* NP = new StockNode(S);
// A NEW STOCKNODE IS DYNAMICALLY CREATED.    
    
    if(length == 0){
        head = NP;
        tail = NP;
        length++;
// IF THE LIST IS EMPTY, THE NEW NODE IS ASSIGNED AS BOTH THE LIST HEAD AND TAIL.        
    }
    else{
        NP->setNext(head);
        head = NP;       
        length++;
// OTHERWISE, THE NEW NODE IS ADDED TO THE HEAD OF THE LIST.        
    }    
}


// THIS FUNCTION SWAPS THE ELEMENTS OF THE TWO PASSED STOCKNODES.
void StockList::swap(StockNode* N1, StockNode* N2){
    
    StockItem* S1 = N1->getElement();
    StockItem* S2 = N2->getElement();
    
    N1->setElement(S2);
    N2->setElement(S1);
}


// THIS FUNCTION SORTS THE INVOKING LIST INTO ITS NATURAL ORDER (UNIT PRICE ASC).
void StockList::sort(){
    
    int swapCount = 0, result = 0;
    
    if(!(length <= 1)){
// IF THE INVOKING LIST IS EMPTY, THE LIST IS ALREADY SORTED, AND SO NOTHING IS
// DONE.        
        
        do{            
            StockNode* N1 = this->getHead();
            StockNode* N2 = N1->getNext();
            swapCount = 0;
            
            for(int i = 0; i < length; i++){
// WE ITERATE THROUGH THE LIST, COMPARING THE NODES IN PAIRS.                
                
                if(i == (length-1)){
                    break;
                }
                
                N2 = N1->getNext();

                result = N1->compare(N2);
                
                if(result == 1){
                    this->swap(N1,N2);
                    swapCount++;
// WE CAN MODIFY THIS CONDITION TO CHANGE THE ORDER. FOR INSTANCE, IF WE WERE
// TO SET THE CONDITION TO -1, WE WOULD GET A DESCENDING LIST.                    
                }
                
                N1 = N2;
            }
            
            if(swapCount == 0){
                break;
// IF THERE ARE NO SWAPS DURING AN ENTIRE ITERATION, THE LIST IS SORTED.            
            }            
        }while(true);
// WE LOOP CONTINUOUSLY UNTIL THE LIST IS SORTED.        
    }    
}


// THIS FUNCTION DELETES THE HEAD OF THE INVOKING STOCKLIST.
void StockList::deleteHead(){
    
    if(length > 0){
// IF THE LIST IS EMPTY, WE DO NOTHING.
        
        StockNode* temp = this->getHead();
// THE HEAD IS TEMPORARILY STORED.

        head = this->getHead()->getNext();
        length--;
// THE NEW HEAD IS THEN ASSIGNED.        

        delete temp;
// WHILST THE MEMORY ASSOCIATED WITH THE PREVIOUS ONE IS RELEASED BACK TO THE 
// PROGRAM.        
    }
}


// THIS FUNCTION DELETES THE TAIL OF THE INVOKING STOCKLIST.
void StockList::deleteTail(){

    if(length > 0){    
// IF THE LIST IS EMPTY, THEN NOTHING IS DONE.        
        
        StockNode* temp = this->getTail();
// THE LOCATION OF THE TAIL IS TEMPORARILY STORED.        

        StockNode* N = this->getHead();

        for(int i = 0; i < length; i++){

            if(N->getNext() == temp){
                tail = N;
                break;
// WE ITERATE THROUGH THE LIST UNTIL WE FIND THE NODE WHICH HAS A NEXT POINTER
// POINTING TO TEMP. THIS IS NOW SET AS THE INVOKING OBJECT'S TAIL.                   
            }

            if(i == length - 1){
                break;
            }

            N = N->getNext();                
        }

        tail->setNext(NULL);
        length--;
// THE INVOKING LIST IS ALTERED LIKE SO.
        
        delete temp;
// WE THEN RELEASE THE MEMORY LOCATION OF TEMP BACK TO THE PROGRAM.    
    }
}


// THIS FUNCTION EMPTIES THE INVOKING STOCKLIST.
void StockList::clear(){
    
    while(length != 0){
        this->deleteHead();
    }
// WE DELETE THE HEAD NODE CONTINOUSLY UNTIL THE LIST IS EMPTY.    
}


// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING STOCKLIST.
void StockList::print(){
    
    StockNode* N = this->getHead();
    
    for(int i = 0; i <= this->getLength()-1; i++){      
        cout << N << endl;     
// WE ITERATE THROUGH THE LIST AND PRINT EACH NODE.        
        
        if(i == (length - 1)){
            break;
        }
        
        N = N->getNext();
    }
// THIS IS THE FUNCTION THAT IS CALLED WHEN A STOCKLIST IS PASSED THROUGH A 
// COUT << CALL.    
}