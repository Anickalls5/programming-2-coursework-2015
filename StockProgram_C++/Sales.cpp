#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string.h>
#include "Sales.h"

// THE SALE CLASS CONSTRUCTOR
Sale::Sale(char* saleDate, int quantity, StockItem* object){    
    
    setDate(saleDate);
    setQuantitySold(quantity);
    itemSold = object;    
// THE SETDATE FUNCTION WILL ENSURE THAT THE DATE IS VALID BEFORE ASSIGNING IT.   
}

// THIS FUNCTION WILL RETURN THE DATE OF THE INVOKING SALE OBJECT.
char* Sale::getDate(){
    return date;
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE DAY VALUE OF THE PASSED
// DATE CHARACTER ARRAY. IT MUST BE NOTED THAT THE CHARACTER ARRAY SHOULD BE IN
// DD/MM/YYYY FORMAT.
int Sale::getDay(char* C){
        
    char* str = C;
    
    int digit1 = ((int)str[0] - 48)*10;
    int digit2 = (int)str[1] - 48;
// THERE IS A DIFFERENCE OF 48 BETWEEN A DIGIT AND ITS ASCII VALUE.    
    
    return digit1+digit2;
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE MONTH VALUE OF THE PASSED
// DATE CHARACTER ARRAY. IT MUST BE NOTED THAT THE CHARACTER ARRAY SHOULD BE IN
// DD/MM/YYYY FORMAT.
int Sale::getMonth(char* C){
    
    char* str = C;
    
    int digit3 = ((int)str[3] - 48)*10;
    int digit4 = (int)str[4] - 48;
// THE VALUES ARE CALCULATED AND THEN ADDED TOGETHER.    
    
    return digit3+digit4;    
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE YEAR VALUE OF THE PASSED
// DATE CHARACTER ARRAY. IT MUST BE NOTED THAT THE CHARACTER ARRAY SHOULD BE IN
// DD/MM/YYYY FORMAT.
int Sale::getYear(char* C){
    
    char* str = C;
    
    int digit5 = ((int)str[6] - 48)*1000;
    int digit6 = ((int)str[7] - 48)*100;
    int digit7 = ((int)str[8] - 48)*10;
    int digit8 = (int)str[9] - 48;
// THE VALUES ARE CALCULATED AND THEN ADDED TOGETHER.    
    
    return digit5+digit6+digit7+digit8;
}


// USING AN IMPLEMENTATION OF ZELLER'S ALGORITHM, AN INTEGER REPRESENTING THE 
// WEEKDAY OF THE PASSED DATE STRING IS RETURNED. (SUNDAY = 0).
int Sale::getWeekday(char* CH){
    
// THIS FUNCTION USES AN IMPLEMENTATION OF ZELLER'S ALGORITHM MENTIONED WITHIN
// THE DECISION MATHEMATICS TEXTBOOK FROM THE MEI MATHEMATICS COLLECTION (3RD ED).    

    int D = this->getDay(CH);
    int M = this->getMonth(CH);
    int Y = this->getYear(CH);
// THE VALUES OF THE DATE ARE FIRST RETRIEVED.    
    
    
    if(M == 1 || M == 2){
        M += 12;
        Y--;
    }
    
    int FY = Y/100;
    int BY = Y%100;
    
    float A = (2.6*M) - 5.39;
    int B = (int)A + (int)(BY/4) + (int)(FY/4) + D + BY;
    int C = B - (2*FY);
    
    int R = C%7;
    
    return R;
// THE WEEKDAY VALUE IS RETURNED.    
}


// THIS FUNCTION RETURNS THE QUANTITY SOLD OF THE INVOKING SALE OBJECT.
int Sale::getQuantitySold(){
    return quantitySold;
}


// THIS FUNCTION RETURNS THE ITEM SOLD OF THE INVOKING SALE OBJECT.
StockItem* Sale::getItemSold(){
    return itemSold;
}


// THIS FUNCTION CALCULATES AND RETURNS THE TOTAL PROFIT OF THE INVOKING SALE
// OBJECT.
int Sale::getProfit(){
    
    StockItem* SP = this->getItemSold();
    
    return (SP->getUnitPrice()*this->getQuantitySold());
    
}


// THIS FUNCTION ASSIGNS THE DATE OF THE INVOKING SALE OBJECT WITH THE PASSED 
// CHARACTER ARRAY.
void Sale::setDate(char* C){
    
    int D = this->getDay(C);
    int M = this->getMonth(C);
    int Y = this->getYear(C);
// THE VALUES OF THE DATE ARE FIRST OBTAINED.    
    
    char str[9];
    char inv[] = "INVALID DATE";
// IF THE DATE PASSED IS NOT VALID, THEN THE ERROR MESSAGE IS COPIED TO THE
// FIELD INSTEAD.    
        
    if(D < 1 || D > 31 || M < 1 || M > 12 || Y < 1980){ 
        strcpy(date,inv);        
    }
    else{    
        int LY = Y%4;
        int valid = 1;
// THE 29TH OF FEBRUARY IS ONLY VALID ON A LEAP YEAR.        
        
        switch(M){
            case 2 : if(D > 28 && LY != 0){ valid = 0; } break;
            case 4 : if(D > 30){ valid = 0; } break;
            case 6 : if(D > 30){ valid = 0; } break;
            case 9 : if(D > 30){ valid = 0; } break;
            case 11: if(D > 30){ valid = 0; } break;
        }
        
        if(valid == 1){        
            str[0] = (char)(D/10)+48;
            str[1] = (char)(D%10)+48;
            str[2] = '/';
            str[3] = (char)(M/10)+48;
            str[4] = (char)(M%10)+48;
            str[5] = '/';
            str[6] = (char)(Y/1000)+48;
            str[7] = (char)((Y/100)%10)+48;
            str[8] = (char)((Y/10)%10)+48;
            str[9] = (char)(Y%10)+48;  
// IF THE DATE IS VALID, THE CHARACTER ARRAY IS POPULATED AND COPIED TO THE 
// DATE FIELD OF THE INVOKING SALE OBJECT.            

            strcpy(this->date,str);
        }
        else{
            strcpy(this->date,inv);
// OTHERWISE, THE ERROR MESSAGE IS COPIED IN ITS PLACE.            
        }
    }
}


// THIS FUNCTION ASSIGNS THE QUANTITY SOLD OF THE INVOKING SALE OBJECT TO THE 
// PASSED INTEGER.
void Sale::setQuantitySold(int Q){
    quantitySold = Q;
}


// THIS FUNCTION ASSIGNS THE ITEM SOLD OF THE INVOKING SALE OBJECT TO THE PASSED
// STOCK ITEM POINTER.
void Sale::setItemSold(StockItem* S){
    itemSold = S;
}


// THIS FUNCTION PRINTS THE INVOKING SALE OBJECT.
void Sale::print(){    
    cout << "DATE: " << left << setw(18) << this->getDate()
         << "ITEM SOLD: " << left << setw(20) << this->getItemSold()->getStockCode()
         << "NUMBER SOLD: " << left << setw(15) << this->getQuantitySold() << endl;
}




// THE SALENODE CLASS CONSTRUCTOR
SaleNode::SaleNode(Sale* S){    
    element = S;    
}


// THIS FUNCTION RETURNS THE SALE NODE POINTED TO BY THE INVOKING OBJECT'S NEXT
// POINTER.
SaleNode* SaleNode::getNext(){
    return next;
}


// THIS FUNCTION RETURNS THE SALE OBJECT ASSIGNED TO THE INVOKING NODE.
Sale* SaleNode::getElement(){
    return element;
}


// THIS FUNCTION ASSIGNS THE NEXT POINTER TO THE PASSED SALE NODE OBJECT.
void SaleNode::setNext(SaleNode* N){    
    next = N;
}


// THIS FUNCTION ASSIGNS THE PASSED SALE OBJECT WITH THE INVOKING SALE NODE.
void SaleNode::setElement(Sale* S){
    element = S;
}


// THIS FUNCTION COMPARES THE INVOKING OBJECT WITH THE PASSED OBJECT IN TERMS
// OF PROFIT. AN INTEGER IS RETURNED THAT SHOWS THE RESULT: 1 IF THE INVOKING 
// NODE HAS A LARGER PROFIT THAN THE PROFIT OF THE PASSED SALE NODE, -1 IF THE 
// INVOKING NODE HAS A SMALLER PROFIT, AND 0 IF THE TWO PROFITS ARE EQUAL.
int SaleNode::compare(SaleNode* N){
        
    int PA = this->getElement()->getProfit();
    int PB = N->getElement()->getProfit();
// THE TWO SALE OBJECTS ARE RETRIEVED AND THE PROFITS CALCULATED.    
    
    if(PA > PB){
        return 1;
    }
    else if(PA < PB){
        return -1;
    }
    else{
        return 0;
    }
}


// THIS FUNCTION PRINTS THE INVOKING SALE NODE.
void SaleNode::print(){    
    cout << this->getElement();
// BECAUSE THE << OPERATOR HAS BEEN OVERRIDDEN, THE CODE ABOVE WILL CALL THE 
// PRINT METHOD OF THE SALE CLASS.    
}




// THE SALELIST CLASS CONSTRUCTOR
SaleList::SaleList(){
    head = NULL;
    tail = NULL;
    length = 0;
}


// THIS FUNCTION RETURNS THE SALE NODE THAT IS POINTED TO BY THE HEAD POINTER OF 
// THE INVOKING SALE LIST OBJECT.
SaleNode* SaleList::getHead(){
    return head;
}


// THIS FUNCTION RETURNS THE SALE NODE THAT IS POINTED TO BY THE TAIL POINTER OF
// THE INVOKING SALE LIST OBJECT.
SaleNode* SaleList::getTail(){
    return tail;
}

// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE NUMBER OF ITEMS WITHIN THE
// INVOKING LIST.
int SaleList::getLength(){
    return length;
}


// THIS FUNCTION ADDS THE PASSED SALE OBJECT TO THE END OF THE INVOKING SALELIST
// OBJECT.
void SaleList::add(Sale* S){
    
    SaleNode* NP = new SaleNode(S);
// A NEW SALENODE IS DYNAMICALLY ALLOCATED MEMORY.    
    
    if(length == 0){
        head = NP;
        tail = NP;        
        length++;
// IF THE LENGTH OF THE INVOKING LIST IS ZERO, THE NEW SALE NODE IS SET AS BOTH
// THE HEAD AND TAIL OF THE INVOKING OBJECT.        
    }
    else{
        tail->setNext(NP);
        tail = NP;  
        length++;
// THE NEW OBJECT IS ADDED TO THE END OF THE LIST.        
    }
}


// THIS FUNCTION ADDS THE PASSED SALE OBJECT TO THE HEAD OF THE INVOKING SALELIST
// OBJECT.
void SaleList::insert(Sale* S){    
    
    SaleNode* NP = new SaleNode(S);
// A NEW SALENODE IS CREATED WITH DYNAMIC MEMORY ALLOCATION.    
    
    if(length == 0){
        head = NP;
        tail = NP;
        length++;
// IF THE LENGTH OF THE INVOKING OBJECT IS ZERO, THE NEW NODE IS SET AS BOTH
// THE HEAD AND TAIL OF THE SALELIST.        
    }
    else{
        NP->setNext(head);
        head = NP;       
        length++;
// OTHERWISE, THE NEW NODE BECOMES THE HEAD OF THE LIST.
    }
}


// THIS FUNCTION SWAPS THE ELEMENTS OF THE TWO PASSED NODES WITHIN THE INVOKING
// SALE LIST.
void SaleList::swap(SaleNode* N1, SaleNode* N2){
    
    Sale* S1 = N1->getElement();
    Sale* S2 = N2->getElement();
    
    N1->setElement(S2);
    N2->setElement(S1);
// THE TWO ELEMENTS ARE SWAPPED.    
}


// THIS FUNCTION SORTS THE INVOKING LIST INTO ITS NATURAL ORDER (PROFIT ASC).
void SaleList::sort(){
    
    int swapCount = 0, result = 0;
    
    if(!(length <= 1)){
// IF THE INVOKING LIST CONTAINS A SINGLE ELEMENT OR FEWER, THEN IT IS ALREADY 
// SORTED.        
        
        do{    
            SaleNode* N1 = this->getHead();
            SaleNode* N2 = N1->getNext();
            swapCount = 0;
            
            for(int i = 0; i < length; i++){
// WE ITERATE THROUGH THE LIST, COMPARING EVERY PAIR OF NODES.                
                
                if(i == (length-1)){
                    break;
                }
                
                N2 = N1->getNext();

                result = N1->compare(N2);
                
                if(result == 1){
                    this->swap(N1,N2);
                    swapCount++;
// IF THE NATURAL ORDER WAS TO BE IN DESCENDING ORDER, WE WOULD CHANGE THE 
// CONDITION SO THAT THE SWAP OCCURRED WHEN THE RESULT WAS -1 RATHER THAN 1.                    
                }
                
                N1 = N2;                
            }
            
            if(swapCount == 0){
                break;
// IF THERE ARE NO SWAPS MADE DURING THE ITERATION, THE LIST IS SORTED.                
            }
            
        }while(true);
// WE WISH TO PERFORM THESE ITERATIONS UNTIL THE LIST IS SORTED.        
    }       
}


// THIS FUNCTION REMOVES THE HEAD NODE FROM THE INVOKING LIST.
void SaleList::deleteHead(){
    
    if(length > 0){
// IF THE LIST IS ALREADY EMPTY, THE FUNCTION SIMPLY DOES NOTHING.
        
        SaleNode* temp = this->getHead();
// WE STORE THE LOCATION OF THE HEAD NODE.
        
        head = this->getHead()->getNext();
        length--;
// WE REASSIGN THE SALELIST'S HEAD NODE.
        
        delete temp;
// THE BLOCK OF MEMORY THAT STORES THE PREVIOUS HEAD NODE IS RELEASED BACK TO 
// THE PROGRAM.        
    }
}


// THIS FUNCTION REMOVES THE TAIL NODE FROM THE INVOKING LIST.
void SaleList::deleteTail(){
    
    if(length > 0){    
// IF THE LIST IS EMPTY, THEN NOTHING IS DONE.        
        
        SaleNode* temp = this->getTail();
// THE LOCATION OF THE TAIL IS TEMPORARILY STORED.        

        SaleNode* N = this->getHead();

        for(int i = 0; i < length; i++){

            if(N->getNext() == temp){
                tail = N;
                break;
// WE ITERATE THROUGH THE LIST UNTIL WE FIND THE NODE WHICH HAS A NEXT POINTER
// POINTING TO TEMP. THIS IS NOW SET AS THE INVOKING OBJECT'S TAIL.                   
            }

            if(i == length - 1){
                break;
            }

            N = N->getNext();                
        }

        tail->setNext(NULL);
        length--;
// THE INVOKING LIST IS ALTERED LIKE SO.
        
        delete temp;
// WE THEN RELEASE THE MEMORY LOCATION OF TEMP BACK TO THE PROGRAM.    
    }
}


// THIS FUNCTION EMPTIES THE INVOKING SALE LIST.
void SaleList::clear(){
    
    while(length != 0){
        this->deleteHead();
        
// WE CONTINUOUSLY DELETE THE HEAD NODE UNTIL THE INVOKING SALE LIST IS EMPTY.        
    }
}


// THIS FUNCTION PRINTS THE CONTENTS OF THE INVOKING SALE LIST.
void SaleList::print(){
    
    SaleNode* N = this->getHead();
    
    for(int i = 0; i <= this->getLength()-1; i++){      
        cout << N;          
// WE ITERATE THROUGH THE LIST AND PRINT EACH NODE. BECAUSE THE << OPERATOR HAS 
// BEEN OVERRIDDEN, THE PRINT FUNCTION OF THE SALENODE CLASS WILL BE CALLED.        
        
        if(i == (length - 1)){
            break;
        }
        
        N = N->getNext();
    }
}


// THIS FUNCTION READS THE sales.txt FILE AND ADDS THEM TO THE INVOKING SALELIST.
// THE PASSED INVENTORY OBJECT ACTS AS A REFERENCE POINT TO THE STOCK ITEMS 
// INCLUDED IN THE SALES.
void SaleList::processTransactions(Inventory* I){
    
    int transactionsProcessed = 0; 
    ifstream str("sales.txt");
    string line;
// THE FILE IS OPENED FOR READING.    
    
    for(;;){        
        getline(str,line);
// THE LINE IS READ AND COPIED INTO THE VARIABLE LINE.        
        
        if(str.eof() == true){
            break;
// IF THE END OF THE FILE HAS BEEN REACHED, WE BREAK OUT OF THE LOOP.            
        }
                
        char fileLine[line.size()];

        for(int i = 0; i < line.size(); i++){
            fileLine[i] = tolower(line[i]);
        }
// THE LINE IS COPIED INTO A CHARACTER ARRAY AND CONVERTED TO LOWERCASE.        
        
        char* saleDate = strtok(fileLine,", ");
        char* stockCode = strtok(NULL,", ");
        char* Q = strtok(NULL,", ");       
// WE THEN SPLIT THE CHARACTER ARRAY INTO THE FIELD VALUES.        
        
        int quantitySold = I->getInt(strtok(Q,"\r"));
// THE CARRIAGE RETURN CHACTER IS REMOVED.
        
        StockItem* SP = I->searchInventory(stockCode);
// WE PERFORM A SEARCH FOR THE ITEM IN THE PASSED INVENTORY OBJECT. IF IT IS NOT
// PRESENT, NULL IS RETURNED.        
        
        if(SP == NULL){
            cout << "TRANSACTION FAILED :: [" << saleDate << "] NO SUCH ITEM (ITEM " << SP->getStockCode() << endl;
            transactionsProcessed++;
            continue;
// IF THE ITEM IS NOT PRESENT IN THE INVENTORY, THE ERROR MESSAGE ABOVE IS PRINTED
// AND THE COUNT IS INCREMENTED.            
        }
        
        Sale* S = new Sale(saleDate,quantitySold,SP);
// A NEW SALE ITEM IS CREATED DYNAMICALLY.        
                
        int stockQuantity = SP->getStockQuantity();        
        int updatedQuantity = stockQuantity - quantitySold;
// WE NOW UPDATE THE STOCK ITEM INCLUDED IN THE SALE BY MODIFYING THE STOCK
// QUANTITY.        
        
        transactionsProcessed++;
        
        if(updatedQuantity < 0){
            cout << "TRANSACTION FAILED :: [" << saleDate << "] OUT OF STOCK (ITEM: " << SP->getStockCode() << ")" << endl;
// IF THE NEW QUANTITY BECOMES NEGATIVE, THEN THE SALE COULD NOT BE PROCESSED
// COMPLETELY. IT IS ASSUMED THAT IF A SALE CANNOT BE PROCESSED COMPLETELY, THEN
// IT CANNOT BE PROCESSED AT ALL.            
        }
        else{
            SP->setStockQuantity(updatedQuantity);   
// OTHERWISE, THE STOCK ITEM IS UPDATED.
            
            this->add(S);
// AND THEN ADDED TO THE INVOKING SALE LIST. AS SUCH, ONLY SUCCESSFUL TRANSACTIONS
// ARE ADDED TO THE INVOKING LIST.            
        }
    }
    
    cout << length << " TRANSACTIONS PROCESSED SUCCESSFULLY\t" << transactionsProcessed-length << " TRANSACTIONS FAILED" << endl;
// THE STATISTICS OF THIS FUNCTION ARE PRINTED.
}


// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH SOLD THE MOST ITEMS (THE WEEKDAY AND PRICE IS ALSO SHOWN).
void SaleList::findSalesVolumeQuantity(){
    
    char* tempDate = "";
    char* maxDate = "";
    int tempProfit = 0, maxProfit = 0;
    int tempItemCount = 0, maxItemCount = 0;
    Sale* S;
        
    for(SaleNode* N = this->getHead(); N != NULL; N = N->getNext()){
        S = N->getElement();       
        
        if(strcmp(S->getDate(),tempDate) != 0){
            if(tempItemCount > maxItemCount){
                maxProfit = tempProfit;
                maxDate = tempDate;
                maxItemCount = tempItemCount;
            }

            tempProfit = 0;
            tempItemCount = 0;
        }
                    
        StockItem* SIP = S->getItemSold();
            
        tempDate = S->getDate();
        tempProfit += (SIP->getUnitPrice()*S->getQuantitySold());
        tempItemCount += S->getQuantitySold();
    }        
    
    int pounds = maxProfit/100;
    int pence = maxProfit%100;
    
    char* day;
    
    switch(S->getWeekday(maxDate)){
        case 0: day = "SUNDAY"; break;
        case 1: day = "MONDAY"; break;
        case 2: day = "TUESDAY"; break;
        case 3: day = "WEDNESDAY"; break;
        case 4: day = "THURSDAY"; break;
        case 5: day = "FRIDAY"; break;
        case 6: day = "SATURDAY"; break;
    }
    
    cout << "MOST ITEMS SOLD: \tDATE: " << day << "\t" << maxDate << "\t " << maxItemCount << " ITEMS SOLD\t\tPROFIT: £" << pounds << "." << pence << "p" << endl;
}


// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH MADE THE LARGEST PROFIT (THE WEEKDAY AND PRICE IS ALSO SHOWN).
void SaleList::findSalesVolumePrice(){
    
    char* tempDate = "";
    char* maxDate = "";
    int tempProfit = 0, maxProfit = 0;
    int tempItemCount = 0, maxItemCount = 0;
    Sale* S;
        
    for(SaleNode* N = this->getHead(); N != NULL; N = N->getNext()){
        S = N->getElement();       
        
        if(strcmp(S->getDate(),tempDate) != 0){
            if(tempProfit > maxProfit){
                maxProfit = tempProfit;
                maxDate = tempDate;
                maxItemCount = tempItemCount;
            }

            tempProfit = 0;
            tempItemCount = 0;
        }
                    
        StockItem* SIP = S->getItemSold();
            
        tempDate = S->getDate();
        tempProfit += (SIP->getUnitPrice()*S->getQuantitySold());
        tempItemCount += S->getQuantitySold();
    }        
    
    int pounds = maxProfit/100;
    int pence = maxProfit%100;
    
    char* day;
    
    switch(S->getWeekday(maxDate)){
        case 0: day = "SUNDAY"; break;
        case 1: day = "MONDAY"; break;
        case 2: day = "TUESDAY"; break;
        case 3: day = "WEDNESDAY"; break;
        case 4: day = "THURSDAY"; break;
        case 5: day = "FRIDAY"; break;
        case 6: day = "SATURDAY"; break;
    }
    
    cout << "MOST PROFITABLE: \tDATE: " << day << "\t" << maxDate << "\t " << maxItemCount << " ITEMS SOLD\t\tPROFIT: £" << pounds << "." << pence << "p" << endl;
}


// THIS FUNCTION FINDS THE COMPONENT (RESISTOR, TRANSISTOR ETC) WHICH WAS SOLD
// THE MOST.
void SaleList::findMostPopularComponent(){
    
    int Rcount = 0, Ccount = 0, Tcount = 0, Dcount = 0, ICcount = 0;
    
    char* S1 = "resistor";
    char* S2 = "capacitor";
    char* S3 = "transistor";
    char* S4 = "diode";
    char* S5 = "ic";
    
    SaleNode* N = head;
    
    for(int i = 0; i < length; i++){
        
        Sale* SP = N->getElement();
        StockItem* S = N->getElement()->getItemSold();
        
        if(strcmp(S->getComponentType(),S1) == 0){
            Rcount += SP->getQuantitySold();
        }
        else if(strcmp(S->getComponentType(),S2) == 0){
            Ccount += SP->getQuantitySold();
        }
        else if(strcmp(S->getComponentType(),S3) == 0){
            Tcount += SP->getQuantitySold();
        }
        else if(strcmp(S->getComponentType(),S4) == 0){
            Dcount += SP->getQuantitySold();
        }
        else if(strcmp(S->getComponentType(),S5) == 0){
            ICcount += SP->getQuantitySold();
        }
        
// THE APPROPRIATE COUNTS ARE INCREMENTED.        
        
        if(i == length-1){
            break;
        }
        
        N = N->getNext();
    }
    
    int countArr[] = {Rcount,Ccount,Tcount,Dcount,ICcount};
    int maxCount = 0;
    int maxComp = 0;
// AN ARRAY IS CREATED SO THAT THE MAXIMUM CAN BE EASILY FOUND.    
    
    for(int i = 0; i < 5; i++){
        if(countArr[i] > maxCount){
            maxComp = i;
            maxCount = countArr[i];
        }
// WE ITERATE THROUGH THE ARRAY AND ASSIGN THE MAXIMUM VALUE, LIKE SO.        
    }
    
    cout << "MOST POPULAR COMPONENT: ";
    
    switch(maxComp){
        case 0 : cout << "RESISTOR" << endl; break;
        case 1 : cout << "CAPACITOR" << endl; break;
        case 2 : cout << "TRANSISTOR" << endl; break;
        case 3 : cout << "DIODE" << endl; break;
        case 4 : cout << "IC" << endl; break;
    }
// THE RESULT IS PRINTED.    
}