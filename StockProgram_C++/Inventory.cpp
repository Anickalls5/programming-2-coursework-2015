#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string.h>
#include "Inventory.h"
#include "Sales.h"
using namespace std;

// THE CLASS CONSTRUCTOR
Inventory::Inventory(){
    head = NULL;
    tail = NULL;
    length = 0;
}


// THIS FUNCTION READS THE inventory.txt FILE AND ADDS THE STOCK ITEM INFORMATION
// TO THE INVOKING INVENTORY OBJECT.
void Inventory::loadInventory(){
           
    ifstream str("inventory.txt");
    string line;
// THE FILE IS OPENED    
            
    for(;;){
      
        getline(str,line);
// THE LINE IS READ INTO A STRING        
        
        if(str.eof() == true){
            break;
// WE LOOP INDEFINITELY UNTIL THE END OF THE FILE HAS BEEN REACHED
        }
                
        char fileLine[line.size()];
// AN ARRAY OF CHARACTERS, THE SAME SIZE AS THE PROCESSED LINE, IS CREATED

        for(int i = 0; i < line.size(); i++){
            fileLine[i] = tolower(line[i]);
// THE LINE IS COPIED AND CONVERTED TO LOWERCASE            
        }

        char* componentType = strtok(fileLine,", ");
        char* stockCode = strtok(NULL,", ");
        char* stockQuantity = strtok(NULL,", ");
        char* unitPrice = strtok(NULL,", ");
        char* componentInfo = strtok(NULL,", ");
// WE CAN NOW SPLIT THE CHARACTER ARRAY INTO THE SEPARATE FIELDS
        
        if(strcmp(componentType,"resistor") == 0){

            char* resistance = strtok(componentInfo,"\r");

            Resistor* R = new Resistor(stockCode,this->getInt(stockQuantity),this->getInt(unitPrice),resistance);
            this->add(R);
// IF THE COMPONENT IS A RESISTOR, WE USE THE RESISTOR CLASS CONSTRUCTOR LIKE SO            
        }
        else if(strcmp(componentType,"capacitor") == 0){

            char* capacitance = strtok(componentInfo,"\r");

            Capacitor* C = new Capacitor(stockCode,this->getInt(stockQuantity),this->getInt(unitPrice),capacitance);
            this->add(C);
// WE ALSO USE A CAPACITOR CLASS CONSTRUCTOR IF THE COMPONENT TYPE IS A CAPACITOR.            
        }
        else if(strcmp(componentType,"transistor") == 0){

            char* transistorType = strtok(componentInfo,"\r");

            Transistor* T = new Transistor(stockCode,this->getInt(stockQuantity),this->getInt(unitPrice),transistorType);
            this->add(T);
// THE TRANSISTOR CLASS CONSTRUCTOR IS CALLED HERE.            
        }
        else if(strcmp(componentType,"ic") == 0){

            char* icDescription = strtok(componentInfo,"\r");

            IC* I = new IC(stockCode,this->getInt(stockQuantity),this->getInt(unitPrice),icDescription);
            this->add(I);
// THE IC CLASS CONSTRUCTOR IS CALLED HERE.
        }
        else if(strcmp(componentType,"diode") == 0){
            
            char* price = strtok(unitPrice,"\r");
            
            Diode* D = new Diode(stockCode,this->getInt(stockQuantity),this->getInt(price));
            this->add(D);
// IF THE COMPONENT TYPE IS A DIODE, WE REMOVE THE CARRIAGE RETURN CHARACTER FROM
// THE UNIT PRICE AND CALL THE DIODE CLASS CONSTRUCTOR.           
        }
        else{
            cout << "THIS ITEM WAS NOT ADDED" << endl;
        }
    }
    
    str.close();
// WHEN THE END OF THE FILE HAS BEEN REACHED, WE CLOSE THE STREAM.    
}


// THIS FUNCTION RETURNS THE INTEGER EQUIVALENT OF A PASSED CHARACTER ARRAY, C.
int Inventory::getInt(char* C){
    
    int length = strlen(C);
    int total = 0;
    int value;
    int base = 1;
    char digit;
    
    for(int i = length-1; i >= 0; i--){
        digit = C[i];
        value = (int)digit - 48;  
        
        total += value*base;
        base *= 10;
// WE ITERATE THROUGH THE ARRAY IN REVERSE ORDER, DERIVING THE VALUE OF EACH
// CHARACTER WITHIN THE ARRAY.        
    }
    
    return total;
// THE INTEGER VALUE OF THE CHARACTER ARRAY IS RETURNED.    
}


// THIS FUNCTION ITERATES THROUGH THE INVOKING INVENTORY AND COUNTS ALL 
// COMPONENTS OF TYPE C (RESISTOR, CAPACITOR, DIODE, TRANSISTOR, IC). THIS 
// FUNCTION COUNTS THE NUMBER OF COMPONENTS, NOT THE NUMBER OF TYPES OF THE 
// COMPONENT.
int Inventory::countComponent(char* C){
    
    int count = 0;
    char x[strlen(C)];
// WE FIRST CREATE A CHARACTER ARRAY OF THE SAME SIZE AS C.    
    
    for(int i = 0; i < strlen(C)+1; i++){
        x[i] = tolower(C[i]);
// WE THEN COPY C AND CONVERT IT TO LOWERCASE FOR EASE OF COMPARISON.        
    }    
    
    char* S1 = "resistor";
    char* S2 = "capacitor";
    char* S3 = "transistor";
    char* S4 = "ic";
    char* S5 = "diode";
    
    if(strcmp(x,S1) != 0 && strcmp(x,S2) != 0 && strcmp(x,S3) != 0 && strcmp(x,S4) != 0 && strcmp(x,S5) != 0){
        cout << "PLEASE SELECT FROM ONE OF THE FOLLOWING COMPONENT TYPES: resistor, transistor, diode, ic, capacitor" << endl;
        return 0;
// IF THE PASSED CHARACTER ARRAY IS NOT ONE OF THE FIVE STRINGS ABOVE, THEN AN
// ERROR MESSAGE IS PRINTED AND THE FUNCTION RETURNS ZERO.        
    }
    else{
        
        StockNode* N = this->getHead();
        
        for(int j = 0; j < length; j++){
// OTHERWISE, WE ITERATE THROUGH THE INVOKING INVENTORY            
            
            StockItem* currentItem = N->getElement();
            
            if(strcmp(currentItem->getComponentType(),x) == 0){
                count += currentItem->getStockQuantity();
// IF THE COMPONENT TYPE MATCHES THE CHARACTER ARRAY PASSED, WE INCREMENT THE 
// COUNT BY THE NUMBER OF ITEMS IN STOCK.         
            }
            
            if(j == (length - 1)){
                break;
// THIS PREVENTS N FROM BEING NULL AND AN ERROR FROM OCCURRING.                
            }
            
            N = N->getNext();
        }
        
        return count;
// THE COUNT IS RETURNED.        
    }
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE NUMBER OF STOCK ITEMS WITH
// AT LEAST ONE ITEM IN STOCK. IT DOES NOT COUNT THE NUMBE OF ITEMS IN THE 
// INVOKING INVENTORY.
int Inventory::countItemsInStock(){
    
    int count = 0;
    
StockNode* N = this->getHead();
        
    for(int i = 0; i < length; i++){
// WE ITERATE THROUGH THE INVOKING INVENTORY OBJECT.

        StockItem* currentItem = N->getElement();

        if(currentItem->getStockQuantity() > 0){
            count++;
// IF THE STOCK QUANTITY IS LARGER THAN ZERO, WE INCREMENT THE COUNT.            
        }

        if(i == (length - 1)){
            break;
// THIS PREVENTS N FROM BEING NULL AND AN ERROR FROM OCCURRING.            
        }

        N = N->getNext();
    }

    return count;    
// THE COUNT IS RETURNED.    
}


// THIS FUNCTION ITERATES THROUGH THE INVOKING INVENTORY AND RETURNS THE STOCK
// ITEM WHICH HAS A STOCK CODE MATCHING THE CHARACTER ARRAY, C.
StockItem* Inventory::searchInventory(char* C){

    char code[strlen(C)];
// WE CREATE A NEW CHARACTER ARRAY THE SAME SIZE AS C.    
    
    for(int i = 0; i < strlen(C)+1; i++){
        code[i] = tolower(C[i]);
// WE THEN COPY AND CONVERT THE PASSED CHARACTER ARRAY INTO LOWERCASE.        
    }
    
    StockNode* N = head;
    
    for(int i = 0; i < length; i++){
// WE NOW ITERATE THROUGH THE INVOKING INVENTORY.
        
        StockItem* S = N->getElement();

        if(strcmp(S->getStockCode(),code) == 0){
            return S;
// IF THE STOCK CODE OF THE ITEM IN THE CURRENT NODE MATCHES THE PASSED CHARACTER
// ARRAY, WE RETURN THE STOCK ITEM OBJECT.            
        }
        
        if(i == length-1){
            break;
// THIS PREVENTS N FROM BEING NULL AND AN ERROR FROM OCCURRING.             
        }
        
        N = N->getNext();
    }
    return NULL;
// IF THERE IS NO ITEM WITH THE PASSED STOCK CODE, NULL IS RETURNED.    
}


// THIS FUNCTION ADDS TOGETHER THE RESISTANCE VALUES OF ALL RESISTORS THAT ARE 
// CURRENTLY IN STOCKIN THE INVOKING INVENTORY OBJECT.
void Inventory::findTotalResistance(){
    
    unsigned int integerTotal = 0;
    int fractionTotal = 0;
// THE TOTAL WILL BE SPLIT INTO TWO PARTS, AN INTEGER PART AND A FRACTIONAL PART.    
        
    for(StockNode* N = this->getHead(); N != NULL; N = N->getNext()){
// WE FIRST ITERATE THROUGH THE INVOKING INVENTORY LIST.        
        
        StockItem* S = N->getElement();
                
        if(strcmp(S->getComponentType(),"resistor") != 0 || S->getStockQuantity() == 0){           
            continue;
// IF THE CURRENT ITEM IN THE ITERATION IS NOT A RESISTOR OR THERE ARE NO MORE 
// IN STOCK, WE MOVE ONTO THE NEXT STOCK NODE.            
        } 
        
        char* C = S->getComponentInfo();
// THE RESISTANCE IS OBTAINED.        
        
        stringstream line(C);
        int X = 0,Y = 0,Z = 0;
        char base;

        line >> X >> base >> Y;
// WE SPLIT THE CHARACTER ARRAY INTO THREE COMPONENTS: THE UNIT, THE INTEGER
// PART AND THE DECIMAL PART.        

        if(base == 'R' || base == 'r'){
            Z = Y;
            Y = 0;
// IF THE UNIT IS 'R', THEN THE RESISTANCE WILL CONTAIN A DECIMAL VALUE. WE 
// SET THE VALUE OF Z TO THE VALUE OF Y, WHICH CURRENTLY HOLDS THE SECOND DIGIT.            
        }        
        else if(base == 'K' || base == 'k'){
            X *= 1000;
            Y *= 100;
// IF THE UNIT IS 'K', THEN WE MULTIPLY THE FIRST DIGIT, X, BY 1000 AND THE 
// SECOND DIGIT, Y, BY 100.            
        }
        else if(base == 'M' || base == 'm'){
            X *= 1000000;
            Y *= 100000;
// IF THE UNIT IS 'M', THEN WE MULTIPLY THE FIRST DIGIT, X, BY 1000000 AND THE 
// SECOND DIGIT, Y, BY 100000.            
        }

        integerTotal += S->getStockQuantity()*X;
        integerTotal += S->getStockQuantity()*Y;
        fractionTotal += S->getStockQuantity()*Z;
// WE INCREMENT THE INTEGER AND FRACTION TOTALS LIKE SO.        
        
        if(fractionTotal >= 10){
            integerTotal += fractionTotal/10;
            fractionTotal = fractionTotal%10;
// THE SMALLEST UNIT OF RESISTANCE WILL ONLY BE TO ONE DECIMAL PLACE. WE MODIFY
// THE FRACTION TOTAL LIKE SO TO PRESERVE THIS.            
        }
    }
    
    cout << "TOTAL RESISTANCE: " << integerTotal << "." << fractionTotal << " OHMS" << endl;   
// THE RESULT IS THEN PRINTED.    
}


// THIS FUNCTION ADDS TOGETHER THE CAPACITANCE VALUES OF ALL CAPACITORS THAT ARE
// IN STOCK WITHIN THE INVOKING INVENTORY OBJECT. THE RESULT IS IN PICOFARADS.
void Inventory::findTotalCapacitance(){
    
    unsigned int total = 0;
        
    for(StockNode* N = this->getHead(); N != NULL; N = N->getNext()){        
        
        StockItem* S = N->getElement();
        
        if(strcmp(S->getComponentType(),"capacitor") != 0 || S->getStockQuantity() == 0){
            continue;
// WE ITERATE THROUGH THE INVOKING INVENTORY. IF THE CURRENT STOCK NODE IS NOT 
// A CAPACITOR, OR THE STOCK QUANTITY IS ZERO, WE MOVE ONTO THE NEXT STOCK NODE.            
        } 
        
        char* C = S->getComponentInfo();
// THE CAPACITANCE STRING IS RETRIEVED.        
        
        stringstream line(C);
        int X;
        char base;

        line >> X >> base;
// THE CAPACITANCE IS NOW SPLIT INTO A VALUE AND A UNIT.        
     
        if(base == 'n' || base == 'N'){
            X *= 1000;
// THIS IS THE CONVERSION FROM NANOFARADS TO PICOFARADS.            
        }
        else if(base == 'u' || base == 'U'){
            X *= 1000000;
// THIS IS THE CONVERSION FROM MICROFARADS TO PICOFARADS.
        }

        total += X;       
// THE CAPACITANCE TOTAL IS INCREMENETED.        
    }

    cout << "TOTAL CAPACITANCE: " << total << " PICOFARADS (pF)" << endl;
// THE RESULT IS RETURNED.    
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE NUMBER OF TRANSISTORS OF
// TYPE T THAT ARE IN STOCK. THIS FUNCTION DOES NOT COUNT THE NUMBER OF TRANSISTOR
// TYPES.
int Inventory::countTransistors(char* T){
    
    int count = 0;
    char x[strlen(T)];   
    
    for(int i = 0; i < strlen(T)+1; i++){
        x[i] = tolower(T[i]);
// THE PASSED CHARACTER ARRAY IS COPIED AND CONVERTED TO LOWERCASE.        
    }    
    
    char* S1 = "npn";
    char* S2 = "pnp";
    char* S3 = "fet";
    
    if(strcmp(x,S1) != 0 && strcmp(x,S2) != 0 && strcmp(x,S3) != 0){
        cout << "PLEASE SELECT FROM ONE OF THE FOLLOWING COMPONENT TYPES: resistor, transistor, diode, ic, capacitor" << endl;
        return 0;
// IF THE PASSED TYPE IS NOT ONE OF THE THREE ABOVE, THEN ZERO IS RETURNED.        
    }
    else{        
        StockNode* N = this->getHead();
        
        for(int j = 0; j < length; j++){
            
            StockItem* currentItem = N->getElement();
            
            if(strcmp(currentItem->getComponentType(),"transistor") == 0 && strcmp(currentItem->getComponentInfo(),x) == 0){
                count += currentItem->getStockQuantity();
            }
            
            if(j == (length - 1)){
                break;
            }
            
            N = N->getNext();
        }
        
        return count;
    }    
}