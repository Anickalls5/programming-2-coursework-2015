#ifndef INVENTORY_H
#define INVENTORY_H
#include "StockItem.h"
using namespace std;

// THIS FILE DEFINES THE INVENTORY CLASS AS A SUBCLASS OF STOCKLIST.

class Inventory : public StockList {
    
    public:
        
// THE CLASS CONSTRUCTOR        
        Inventory();
  
        
// THIS FUNCTION READS THE inventory.txt FILE AND ADDS THE CONTENTS TO THE LIST        
        void loadInventory();
        
        
// THIS FUNCTION RETURNS THE INTEGER REPRESENTATION OF THE PASSED CHARACTER ARRAY        
        int getInt(char* C);
        
        
// THIS FUNCTION RETURNS A COUNT OF THE NUMBER OF COMPONENTS, SPECIIFIED BY C.        
        int countComponent(char* C);
        
        
// THIS FUNCTION RETURNS A COUNT OF THE NUMBER OF ITEMS THAT ARE IN STOCK.        
        int countItemsInStock();
        
        
// THIS FUNCTION SEARCHES THE INVOKING OBJECT FOR THE STOCK ITEM WITH THE CODE, C        
        StockItem* searchInventory(char* C);
                
        
// THIS FUNCTION CALCULATES THE TOTAL RESISTANCE OF ALL RESISTORS IN STOCK        
        void findTotalResistance();
        
        
// THIS FUNCTION CALCULATES THE TOTAL CAPACITANCE OF ALL CAPACITORS IN STOCK        
        void findTotalCapacitance();
        
        
// THIS FUNCTION RETURNS A COUNT ON THE NUMBER OF TRANSISTORS WITH TYPE T        
        int countTransistors(char* T); 
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING        
        friend ostream& 
        operator<<(ostream& str, Inventory I){
            I.print();
            return str;
        }          
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)        
        friend ostream& 
        operator<<(ostream& str, Inventory* I){
            I->print();
            return str;
        }           
        
};

#endif