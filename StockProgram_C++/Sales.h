#ifndef SALES_H
#define SALES_H
#include <iostream>
#include "Inventory.h"
#include "StockItem.h"

using namespace std;

// THIS FILE DEFINES THE SALE, THE SALENODE AND SALELIST CLASSES.

class Sale {
    
    private:
        
        char date[13];
        int quantitySold;
        StockItem* itemSold;
        
    public:
        
// THE CLASS CONSTRUCTOR        
        Sale(char* saleDate, int quantity, StockItem* object);
        
        
// THIS FUNCTION RETURNS THE DATE OF THE INVOKING SALE OBJECT.        
        char* getDate();
        
        
// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE DAY OF THE PASSED DATE, C.        
        int getDay(char* C);
        
        
// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE MONTH OF THE PASSED DATE, C.      
        int getMonth(char* C);
        
        
// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE YEAR OF THE PASSED DATE, C.        
        int getYear(char* C);
        
        
// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE WEEKDAT OF THE PASSED DATE.      
        int getWeekday(char* S);
        
        
// THIS FUNCTION RETURNS THE QUANTITY SOLD OF THE INVOKING SALE OBJECT.        
        int getQuantitySold();
                
        
// THIS FUNCTION RETURNS THE PROFIT OF THE INVOKING SALE OBJECT.        
        int getProfit();        
        
        
// THIS FUNCTION RETURNS THE ITEMSOLD OF THE INVOKING SALE OBJECT.        
        StockItem* getItemSold();
        
        
// THIS FUNCTION ASSIGNS THE DATE TO THE PASSED CHARACTER ARRAY, C.        
        void setDate(char* C);
        
        
// THIS FUNCTION ASSIGNS THE QUANTITY SOLD TO THE PASSED INTEGER, Q.        
        void setQuantitySold(int Q);
        
        
// THIS FUNCTION ASSIGNS THE ITEM SOLD TO THE PASSED STOCK ITEM POINTER.        
        void setItemSold(StockItem* S);
        
        
// THIS FUNCTION PRINTS THE INVOKING SALE OBJECT.        
        void print();    
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING    
        friend ostream& 
        operator<<(ostream& str, Sale S){
            S.print();
            return str;            
        }          
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)           
        friend ostream& 
        operator<<(ostream& str, Sale* S){
            S->print();
            return str;            
        }            
};

// LINKED LIST CLASSES *********************************************************

class SaleNode {
    
// THIS IS THE FIRST OF TWO CLASSES THAT ACT AS A LINKEDLIST IMPLEMENTATION FOR 
// THE PROGRAM.
    
    private:
        
        Sale* element;
        SaleNode* next;
        
    public:

// THE CLASS CONSTRUCTOR                
        SaleNode(Sale* S);
        
        
// THIS FUNCTION RETURNS THE NODE THAT IS POINTED TO BY THIS NODE'S NEXT POINTER.        
        SaleNode* getNext();
        
        
// THIS FUNCTION RETURNS THE SALE ITEM ASSOCIATED WITH THE INVOKING SALENODE.        
        Sale* getElement();
        
        
// THIS FUNCTION ASSIGNS THE PASSED SALENODE TO THE NEXT POINTER OF THIS OBJECT.        
        void setNext(SaleNode* N);
        
        
// THIS FUNCTION ASSIGNS THE PASSED SALE POINTER TO THE INVOKING SALENODE.        
        void setElement(Sale* S);
  
        
// THIS FUNCTION COMPARES THE INVOKING AND PASSED SALENODES BASED ON PROFIT.
        int compare(SaleNode* N);
        
        
// THIS FUNCTION PRINTS THE INVOKING SALENODE.        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING           
        friend ostream& 
        operator<<(ostream& str, SaleNode S){
            S.print();
        }          
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)           
        friend ostream& 
        operator<<(ostream& str, SaleNode* S){
            S->print();
        }          
        
};


class SaleList {

// THIS IS THE SECOND OF TWO CLASSES THAT ACT AS A LINKEDLIST IMPLEMENTATION FOR
// THE PROGRAM.    
    
    private:
    
        SaleNode* head;
        SaleNode* tail;
        int length;
        
    public:
        
// THE CLASS CONSTRUCTOR        
        SaleList();
        
        
// THIS FUNCTION RETURNS THE SALENODE REPRESENTING THE HEAD OF THE INVOKING LIST.       
        SaleNode* getHead();
        
        
// THIS FUNCTION RETURNS THE SALENODE REPRESENTING THE TAIL OF THE INVOKING LIST.             
        SaleNode* getTail();
                
        
// THIS FUNCTION RETURNS THE LENGTH OF THE INVOKING LIST.
        int getLength();
        
        
// THIS FUNCTION ADDS THE PASSED SALE TO THE END OF THE INVOKING LIST.        
        void add(Sale* N);
        
        
// THIS FUNCTION ADDS THE PASSED SALE TO THE HEAD OF THE INVOKING LIST.        
        void insert(Sale* N);
        
        
// THIS FUNCTION SWAPS THE POSITIONS OF THE PASSED SALENODES.        
        void swap(SaleNode* N1, SaleNode* N2);
        
        
// THIS FUNCTION WILL SORT THE INVOKING SALELIST INTO ITS NATURAL ORDER.        
        void sort();
        
        
// THIS FUNCTION REMOVES THE HEAD NODE FROM THE INVOKING LIST.        
        void deleteHead();
        
        
// THIS FUNCTION REMOVES THE TAIL NODE FROM THE INVOKING LIST.        
        void deleteTail();
        
        
// THIS FUNCTION EMPTIES THE INVOKING LIST.        
        void clear();
        
        
// THIS FUNCTION PRINTS ALL ELEMENTS WITHIN THE INVOKING LIST.        
        void print();
        
        
// THIS FUNCTION READS THE sale.txt FILE AND ADDS THE SALES TO THE LIST.        
        void processTransactions(Inventory* I);
                        
        
// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH SOLD THE MOST ITEMS (THE WEEKDAY AND PRICE IS ALSO SHOWN).
        void findSalesVolumeQuantity();
        
        
// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH MADE THE LARGEST PROFIT (THE WEEKDAY AND PRICE IS ALSO SHOWN).
        void findSalesVolumePrice();        
        
        
// THIS FUNCTION FINDS THE COMPONENT WHICH APPEARS IN THE MOST SALES.        
        void findMostPopularComponent();           
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING   
        friend ostream& 
        operator<<(ostream& str, SaleList S){
            S.print();
        }          
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)           
        friend ostream& 
        operator<<(ostream& str, SaleList* S){
            S->print();
        }          
    
};

#endif