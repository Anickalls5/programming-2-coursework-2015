#ifndef STOCKITEM_H
#define STOCKITEM_H
#include <iomanip>
using namespace std;

// SUPERCLASS ******************************************************************

class StockItem {
        
// THIS FUNCTION DEFINES THE ABSTRACT STOCKITEM CLASS, AS WELL AS ITS FIVE 
// CONCRETE SUBCLASSES AND THE SALENODE AND SALELIST CLASSES.    
    
    protected:
    
        char componentType[15];
        char stockCode[15];
        int stockQuantity;
        int unitPrice;
        char componentInfo[15];
        
    public:
        
// ACCESSOR FUNCTIONS **********************************************************        
        
// THIS FUNCTION RETURNS THE COMPONENT TYPE.        
        char* getComponentType();
        
        
// THIS FUNCTION RETURNS THE STOCK CODE.
        char* getStockCode();
        
        
// THIS FUNCTION RETURNS THE STOCK QUANTITY.        
        int getStockQuantity();
        
        
// THIS FUNCTION RETURNS THE UNIT PRICE.        
        int getUnitPrice();            
                
        
// THIS PURE VIRTUAL FUNCTION WILL BE OVERRIDDEN TO RETURN THE COMPONENT INFORMATION.
        virtual char* getComponentInfo() = 0;
        
// MUTATOR FUNCTIONS ***********************************************************        
        
// THIS FUNCTION ASSIGNS THE COMPONENT TYPE TO THE CHARACTER ARRAY, S.        
        void setComponentType(char* S);
    
        
// THIS FUNCTION ASSIGNS THE STOCK CODE TO THE CHARACTER ARRAY, S.        
        void setStockCode(char* S);
        
        
// THIS FUNCTION ASSIGNS THE STOCK QUANTITY TO THE PASSED INTEGER, I.        
        void setStockQuantity(int I);
        
        
// THIS FUNCTION ASSIGNS THE UNIT PRICE TO THE PASSED INTEGER, I.        
        void setUnitPrice(int I);
        
        
// THIS PURE VIRTUAL FUNCTION WILL BE OVERRIDDEN TO SET THE COMPONENT INFORMATION.        
        virtual void setComponentInfo(char* S) = 0;
        
        
// THIS PURE VIRTUAL FUNCTION WILL BE OVERRIDDEN TO PRINT DIFFERENT OBJECTS.        
        virtual void print() = 0;
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)           
        friend ostream& 
        operator<<(ostream& str, StockItem* S){
            S->print();
            return str;            
        }          
                
};

// SUBCLASSES ******************************************************************

class Diode : public StockItem {
    
    public:         

// THE CLASS CONSTRUCTOR        
        Diode(char* code, int quantity, int price);

        
// THIS FUNCTION RETURNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN).
        char* getComponentInfo() override;
        
        
// THIS FUNCTION ASSIGNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN).      
        void setComponentInfo(char* S) override;    
        
        
// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING OBJECT.        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING           
        friend ostream& 
        operator<<(ostream& str, Diode D){
            D.print();
            return str;            
        }  
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)           
        friend ostream& 
        operator<<(ostream& str, Diode* D){
            D->print();
            return str;            
        }          
    
};

class Resistor : public StockItem{
        
    public:
           
// THE CLASS CONSTRUCTOR        
        Resistor(char* code, int quantity, int price, char* res);

        
// THIS FUNCTION RETURNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)        
        char* getComponentInfo() override;
        
        
// THIS FUNCTION ASSIGNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)        
        void setComponentInfo(char* S) override;
        
        
// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING OBJECT.
        void print();        
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING.
        friend ostream& 
        operator<<(ostream& str, Resistor R){
            R.print();
            return str;            
        }          
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)          
        friend ostream& 
        operator<<(ostream& str, Resistor* R){
            R->print();
            return str;            
        }          
};

class Capacitor : public StockItem{
            
    public:    
        
// CLASS CONSTRUCTOR        
        Capacitor(char* code, int quantity, int price, char* cap);

        
// THIS FUNCTION RETURNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN) 
        char* getComponentInfo();

        
// THIS FUNCTION ASSIGNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)         
        void setComponentInfo(char* S);
        
        
// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING OBJECT.        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING    
        friend ostream& 
        operator<<(ostream& str, Capacitor C){
            C.print();
            return str;            
        }  

        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)            
        friend ostream& 
        operator<<(ostream& str, Capacitor* C){
            C->print();
            return str;            
        }         
        
};

class Transistor : public StockItem{
        
    public:     

// THE CLASS CONSTRUCTOR        
        Transistor(char* code, int quantity, int price, char* tType);

        
// THIS FUNCTION RETURNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)      
        char* getComponentInfo();
       
        
// THIS FUNCTION ASSIGNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)              
        void setComponentInfo(char* S);
        
        
// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING OBJECT.        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING.       
        friend ostream& 
        operator<<(ostream& str, Transistor T){
            T.print();
            return str;            
        }   
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS).         
        friend ostream& 
        operator<<(ostream& str, Transistor* T){
            T->print();
            return str;            
        }           
        
};

class IC : public StockItem{
        
    public:      
        
// THE CLASS CONSTRUCTOR        
        IC(char* code, int quantity, int price, char* desc);

        
// THIS FUNCTION RETURNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)          
        char* getComponentInfo();

        
// THIS FUNCTION ASSIGNS THE COMPONENT INFORMATION (VIRTUALS MUST BE OVERRIDDEN)          
        void setComponentInfo(char* S);
        
        
// THIS FUNCTION PRINTS THE INFORMATION OF THE INVOKING OBJECT.        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING.      
        friend ostream& 
        operator<<(ostream& str, IC I){
            I.print();
            return str;            
        }

        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS).        
        friend ostream& 
        operator<<(ostream& str, IC* I){
            I->print();
            return str;            
        }           
        
};

// LINKED LIST CLASSES *********************************************************

class StockNode {
    
// THIS IS THE FIRST OF TWO CLASSES THAT ACT AS A LINKEDLIST IMPLEMENTATION FOR
// THE PROGRAM.    
    
    private:

        StockItem* element;
        StockNode* next;
        
    public:
        
// THE CLASS CONSTRUCTOR        
        StockNode(StockItem* S);
        
        
// THIS FUNCTION RETURNS THE STOCKNODE THAT IS NEXT IN THE LINKEDLIST        
        StockNode* getNext();
        
        
// THIS RETURNS THE STOCKITEM ASSOCIATED WITH THE INVOKING STOCKNODE        
        StockItem* getElement();
        
        
// THIS FUNCTION SETS THE NEXT STOCKNODE POINTER TO THE PASSED STOCKNODE        
        void setNext(StockNode* N);
        
        
// THIS FUNCTION SETS THE STOCKNODE ELEMENT TO THE PASSED STOCKITEM.        
        void setElement(StockItem* S);
        
        
// THIS FUNCTION COMPARES THE INVOKING STOCKNODE WITH THE PASSED STOCKNODE        
        int compare(StockNode* N);
        
        
// THIS FUNCTION PRINTS THE INVOKING STOCKNODE        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING          
        friend ostream& 
        operator<<(ostream& str, StockNode S){
            S.print();
            return str;            
        }    

        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)         
        friend ostream& 
        operator<<(ostream& str, StockNode* S){
            S->print();
            return str;            
        }           

};

class StockList {
    
// THIS IS THE SECOND CLASS THAT ACTS AS AN IMPLEMENTATION OF A LINKEDLIST

    protected:
    
        StockNode* head;
        StockNode* tail;
        int length;
        
    public:
        
// THE CLASS CONSTRUCTOR        
        StockList();
        
        
// THIS FUNCTION RETURNS THE HEAD OF THE INVOKING STOCKLIST        
        StockNode* getHead();
        
        
// THIS FUNCTION RETURNS THE TAIL OF THE INVOKING STOCKLIST        
        StockNode* getTail();
        
        
// THIS FUNCTION RETURNS THE NUMBER OF ITEMS WITHIN THE INVOKING STOCKLIST       
        int getLength();
        
        
// THIS FUNCTION ADDS THE PASSED STOCKITEM TO THE END OF THE STOCKLIST        
        void add(StockItem* S);

        
// THIS FUNCTION INSERTS THE PASSED STOCKITEM TO THE HEAD OF THE STOCKLIST        
        void insert(StockItem* S);
        
        
// THIS FUNCTION SWAPS THE ELEMENTS OF THE TWO PASSED STOCKNODES        
        void swap(StockNode* N1, StockNode* N2);

        
// THIS FUNCTION SORTS THE INVOKING LIST INTO ITS NATURAL ORDER (UNIT PRICE ASC)        
        void sort();
        
        
// THIS FUNCTION DELETES THE HEAD NODE OF THE INVOKING STOCKLIST        
        void deleteHead();
        
        
// THIS FUNCTION DELETES THE TAIL NODE OF THE INVOKING STOCKLIST        
        void deleteTail();
        
        
// THIS FUNCTION EMPTIES THE INVOKING STOCKLIST        
        void clear();
        
        
// THIS FUNCTION PRINTS THE STOCKLIST        
        void print();
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING         
        friend ostream& 
        operator<<(ostream& str, StockList S){
            S.print();
            return str;            
        }            
        
        
// THIS OPERATOR IS OVERLOADED TO PROVIDE A SHORTHAND FOR PRINTING (POINTERS)        
        friend ostream& 
        operator<<(ostream& str, StockList* S){
            S->print();
            return str;            
        }            

};

#endif