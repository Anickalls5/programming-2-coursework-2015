#ifndef SALES_H
#define SALES_H
#include "StockItem.h"

// THIS FILE REPRESENTS THE SALES WITHIN THE PROGRAM AS STRUCTS.

typedef struct saleDT{
    
    StockItem* itemSold;
    char date[11];
    int quantity;   
}
Sale;

// ACCESSOR FUNCTIONS **********************************************************

// RETURNS THE POINTER TO THE STOCK ITEM SOLD
StockItem* getItemSold(Sale* S);

// RETURNS THE SALE DATE
char* getDate(Sale* S);

// RETURNS AN INTEGER REPRESENTING THE DAY VALUE OF THE PASSED DATE STRING
int getDay(char* C);

// RETURNS AN INTEGER REPRESENTING THE MONTH VALUE OF THE PASSED DATE STRING
int getMonth(char* C);

// RETURNS AN INTEGER REPRESENTING THE YEAR VALUE OF THE PASSED DATE STRING
int getYear(char* C);

// RETURNS AN INTEGER REPRESENTING THE WEEKDAY OF THE PASSED STRING (SUNDAY = 0)
int getWeekday(char* C);

// RETURNS THE QUANTITY SOLD
int getQuantitySold(Sale* S);


// MUTATOR FUNCTIONS ***********************************************************

// SETS THE ITEM SOLD TO THE PASSED STOCK ITEM POINTER
void setItemSold(Sale* S, StockItem* SI);

// SETS THE DATE TO THE PASSED CHARACTER ARRAY
void setDate(Sale* S, char* C);

// SETS THE QUANTITY SOLD TO THE PASSED INTEGER ARRAY
void setQuantitySold(Sale* S, int Q);

#endif