#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "StockItem.h"

// ACCESSOR FUNCTIONS **********************************************************

// THIS FUNCTION RETURNS THE COMPONENT TYPE OF THE PASSED STOCK ITEM POINTER.
char* getComponentType(StockItem* S){    
    return S->componentType;
}


// THIS FUNCTION RETURNS THE STOCK CODE OF THE PASSED STOCK ITEM POINTER.
char* getStockCode(StockItem* S){
    return S->stockCode;
}


// THIS FUNCTION RETURNS THE STOCK QUANTITY OF THE PASSED STOCK ITEM POINTER.
int getStockQuantity(StockItem* S){
    return S->stockQuantity;
}


// THIS FUNCTION RETURNS THE UNIT PRICE OF THE PASSED STOCK ITEM POINTER.
int getUnitPrice(StockItem* S){
    return S->unitPrice;
}


// THIS FUNCTION RETURNS COMPONENT INFORMATION (RESISTANCE, TRANSISTOR TYPE ETC).
char* getComponentInfo(StockItem* S){
    return S->componentInfo;
}


// MUTATOR FUNCTIONS ***********************************************************

// THIS FUNCTION SETS THE COMPONENT TYPE OF THE PASSED STOCK ITEM POINTER TO THE 
// PASSED CHARACTER ARRAY, C.
void setComponentType(StockItem* S, char* C){   
    
// THESE STRINGS REPRESENT THE VALUES THAT C CAN TAKE    
    char* str1 = "resistor";
    char* str2 = "capacitor";
    char* str3 = "diode";
    char* str4 = "transistor";
    char* str5 = "IC";
    
    if(strcmp(C,str1) != 0 && strcmp(C,str2) != 0 && strcmp(C,str3) != 0 && strcmp(C,str4) != 0 && strcmp(C,str5) != 0){
        printf("\n\n%s\n\n","PLEASE ENSURE THAT THE COMPONENT TYPE IS SET TO ONE OF THE FOLLOWING: resistor, diode, transistor, capacitor, IC");
// IF THE PASSED CHARACTER ARRAY DOES NOT MATCH ANY OF THE ABOVE STRINGS, PRINT 
// THE ERROR MESSAGE.        
    }
    else{    
        strcpy(S->componentType,C);
// ELSE, IF THE PASSED STRING IS EQUAL TO AT LEAST ONE OF THE FIVE STRINGS ABOVE,
// THE COMPONENT TYPE IS SET TO IT.    
    }
}


// THIS FUNCTION SETS THE STOCK CODE OF THE PASSED STOCK ITEM POINTER TO THE PASSED
// CHARACTER ARRAY, C.
void setStockCode(StockItem* S, char* C){
    strcpy(S->stockCode,C);
}


// THIS FUNCTION SETS THE STOCK QUANTITY OF THE PASSED STOCK ITEM POINTER TO THE 
// PASSED INTEGER, I.
void setStockQuantity(StockItem* S, int I){
    S->stockQuantity = I;
}


// THIS FUNCTION SETS THE UNIT PRICE OF THE PASSED STOCK ITEM POINTER TO THE PASSED
// INTEGER, I.
void setUnitPrice(StockItem* S, int I){
    S->unitPrice = I;
}


// THIS FUNCTION SETS THE COMPONENT INFORMATION OF THE PASSED STOCK ITEM POINTER
// TO THE PASSED CHARACTER ARRAY, C.
char* setComponentInfo(StockItem* S, char* C){

// THESE STRINGS ARE USED FOR THE COMPARISON OPERATIONS BELOW    
    char* str = "transistor";
    char* str1 = "NPN";
    char* str2 = "PNP";
    char* str3 = "FET";
       
    if(strcmp(getComponentType(S),str) == 0 && strcmp(C,str1) != 0 && strcmp(C,str2) != 0 && strcmp(C,str3)){
        printf("\n\n%s\n\n","PLEASE ENSURE THAT THE COMPONENT TYPE IS SET TO ONE OF THE FOLLOWING: NPN, PNP, FET");
// IF THE COMPONENT TYPE OF THE PASSED STOCK ITEM POINTER IS NOT A TRANSISTOR, OR
// THE COMPONENT INFORMATION DOES NOT EQUAL ONE OF THE ABOVE TYPES, AN ERROR 
// MESSAGE IS PRINTED.        
    }
    else{    
        strcpy(S->componentInfo,C);
// IF THE STOCK ITEM POINTER IS A TRANSISTOR AND HAS A TRANSISTOR TYPE THAT MATCHES
// ONE OF THE THREE ABOVE, THE COMPONENT INFORMATION IS SET.        
    }
}