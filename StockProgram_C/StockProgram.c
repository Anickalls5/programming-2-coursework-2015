#include <stdio.h>
#include <stdlib.h>
#include "Inventory.h";

// THIS FUNCTION READS THE sales.txt FILE AND PROCESSES THESE TRANSACTIONS,
// USING A LIST AS AN INVENTORY REFERENCE WHERE THE QUANTITIES WILL BE MODIFIED.
List* processTransactions(List* inventory){
    
    int transactionsProcessed = 0;
    
    List* LP = createList();
// THE LIST IS CREATED.    
    
    FILE* FP;
    FP = fopen("sales.txt","r");
// THE SALES FILE IS OPENED.    
    
    char buffer[200];
    
    while(fgets(buffer,200,FP) != NULL){
// AS LONG AS THE END OF THE FILE HAS NOT BEEN REACHED, DO THE FOLLOWING:        
        
        char* saleDate = strtok(buffer,", ");
        char* stockCode = strtok(NULL,", ");
        char* Q = strtok(NULL,", ");        
// THE LINE STRING IS SPLIT ACCORDINGLY.        
        
        int quantitySold = getInt(strtok(Q,"\r"));
// THE CARRIAGE RETURN CHARACTER (\r) IS REMOVED.        
        
        Sale* S = malloc(sizeof(Sale));
        StockItem* SP = searchInventory(inventory,stockCode);
// THE ITEM SOLD IN THE SALE IS RETRIEVED FROM THE INVENTORY, IF IT IS NULL, DO
// THE FOLLOWING:        
        
        if(SP == NULL){
            printf("TRANSACTION FAILED :: [%s] NO SUCH ITEM (ITEM %s)\n",saleDate,getStockCode(SP));
            transactionsProcessed++;
            continue;
// IF THERE IS NO ITEM WITHIN THE INVENTORY, THE TRANSACTION HAS FAILED (THOUGH
// IT HAS BEEN PROCESSED, SO THE APPROPRIATE VARIABLE IS INCREMENTED).            
        }
        
        setDate(S,saleDate);
        setItemSold(S,SP);
        setQuantitySold(S,quantitySold);
// THE PROPERTIES OF THE SALE OBJECT ARE ASSIGNED.        
        
        int stockQuantity = getStockQuantity(SP);        
        int updatedQuantity = stockQuantity - quantitySold;
// THE STOCK OBJECT'S QUANTITY IS MODIFIED.        
        
        transactionsProcessed++;
        
        if(updatedQuantity < 0){
            printf("TRANSACTION FAILED :: [%s] OUT OF STOCK (ITEM %s)\n",saleDate,getStockCode(SP));
// AS A STOCK CAN HAVE 0 ITEMS IN STOCK, IT IS ONLY WHEN THE QUANTITY BECOMES
// NEGATIVE THAT AN ERROR MESSAGE IS SHOWN.            
        }
        else{
            setStockQuantity(SP,updatedQuantity);   
// THE STOCK ITEM IS UPDATED WITH THE NEW QUANTITY.
            
            addSaleItem(LP,S);
// THE SALE IS ADDED TO THE SALE LIST AS LONG AS THE TRANSACTION HAS BEEN 
// SUCCESSFUL.            
        }
    }
    
    printf("\n%i%s%i%s",getListLength(LP)," TRANSACTIONS PROCESSED SUCCESSFULLY\t",transactionsProcessed-getListLength(LP)," TRANSACTIONS FAILED\n");
    
    return LP;
// THE LIST OF SALES IS RETURNED.    
}


// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH MADE THE LARGEST PROFIT (THE WEEKDAY AND PRICE ARE ALSO SHOWN).
void findSalesVolumePrice(List* saleList){
    
    char* tempDate = "";
    char* maxDate = "";
    int tempProfit = 0, maxProfit = 0;
    int tempItemCount = 0, maxItemCount = 0;
        
    for(Node* N = saleList->head; N != NULL; N = N->next){
        Sale* S = returnElement(N);       
        
        if(strcmp(getDate(S),tempDate) != 0){
            if(tempProfit > maxProfit){
                maxProfit = tempProfit;
                maxDate = tempDate;
                maxItemCount = tempItemCount;
            }

            tempProfit = 0;
            tempItemCount = 0;
        }
                    
        StockItem* SIP = getItemSold(S);
            
        tempDate = getDate(S);
        tempProfit += (getUnitPrice(SIP)*getQuantitySold(S));
        tempItemCount += getQuantitySold(S);
    }        
    
    int pounds = maxProfit/100;
    int pence = maxProfit%100;
    
    char* day;
    
    switch(getWeekday(maxDate)){
        case 0: day = "SUNDAY"; break;
        case 1: day = "MONDAY"; break;
        case 2: day = "TUESDAY"; break;
        case 3: day = "WEDNESDAY"; break;
        case 4: day = "THURSDAY"; break;
        case 5: day = "FRIDAY"; break;
        case 6: day = "SATURDAY"; break;
    }
    
    printf("\nMOST PROFITABLE: \tDATE: %s\t%s\t %i ITEMS SOLD\t\tPROFIT: £%i.%ip",day,maxDate,maxItemCount,pounds,pence);
}


// THIS FUNCTION ITERATES THROUGH A PASSED LIST OF SALES AND PRINTS OUT THE DATE
// WHICH SOLD THE MOST ITEMS (THE WEEKDAY AND PRICE ARE ALSO SHOWN).
void findSalesVolumeQuantity(List* saleList){
    
    char* tempDate = "";
    char* maxDate = "";
    int tempProfit = 0, maxProfit = 0;
    int tempItemCount = 0, maxItemCount = 0;
        
    for(Node* N = saleList->head; N != NULL; N = N->next){
        Sale* S = returnElement(N);       
        
        if(strcmp(getDate(S),tempDate) != 0){
            if(tempItemCount > maxItemCount){
                maxProfit = tempProfit;
                maxDate = tempDate;
                maxItemCount = tempItemCount;
            }

            tempProfit = 0;
            tempItemCount = 0;
        }
                    
        StockItem* SIP = getItemSold(S);
            
        tempDate = getDate(S);
        tempProfit += (getUnitPrice(SIP)*getQuantitySold(S));
        tempItemCount += getQuantitySold(S);
    }        
    
    int pounds = maxProfit/100;
    int pence = maxProfit%100;
    
    char* day;
    
    switch(getWeekday(maxDate)){
        case 0: day = "SUNDAY"; break;
        case 1: day = "MONDAY"; break;
        case 2: day = "TUESDAY"; break;
        case 3: day = "WEDNESDAY"; break;
        case 4: day = "THURSDAY"; break;
        case 5: day = "FRIDAY"; break;
        case 6: day = "SATURDAY"; break;
    }
    
    printf("\nMOST ITEMS SOLD: \tDATE: %s\t%s\t %i ITEMS SOLD\t\tPROFIT: £%i.%ip",day,maxDate,maxItemCount,pounds,pence);
}

// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE NUMBER OF TRANSISTORS THAT 
// HAVE A TYPE THAT MATCH THE CHARACTER ARRAY, C, WITHIN THE PASSED LIST.
int countTransistors(List* L, char* C){
    
    int count = 0;
    
    for(Node* N = L->head; N != NULL; N = N->next){
        StockItem* S = returnElement(N);
        
        if(strcmp(getComponentType(S),"transistor") == 0 && strcmp(getComponentInfo(S),C) == 0){
            count += getStockQuantity(S);
        }
// WE ITERATE THROUGH THE PASSED LIST, INCREMENTING THE COUNT WHEN WE FIND A 
// STOCK ITEM THAT IS BOTH A TRANSISTOR AND WHICH HAS A COMPONENT TYPE THAT
// MATCHES THE CHARACTER ARRAY, C.
    }
    
    return count;
// THE COUNT VALUE IS THEN RETURNED.
}


// THIS FUNCTION ITERATES THROUGH THE GIVEN LIST AND COLLATES ALL RESISTANCE
// VALUES OF THOSE THAT ARE IN STOCK.
void findTotalResistance(List* inventory){
    
    unsigned int integerTotal = 0;
    int fractionTotal = 0;
    int count = 0;
// UNSIGNED INTEGERS CAN HOLD MUCH LARGER VALUES THAN SIGNED INTEGERS.    
    
    for(Node* N = inventory->head; N != NULL; N = N->next){
        
// WE ITERATE THROUGH THE PASSED LIST AND DO THE FOLLOWING:        
        
        StockItem* S = returnElement(N);
        count++;
        
        if(strcmp(getComponentType(S),"resistor") != 0 || getStockQuantity(S) == 0){                 
            continue;
// WE IGNORE ALL STOCK ITEMS THAT ARE NOT RESISTORS.            
        } 
        
        char* C = getComponentInfo(S);
        int X = 0,Y = 0,Z = 0;
        char base;

        sscanf(C,"%i%c%i",&X,&base,&Y);
// IF THE STOCK ITEM IS A RESISTOR, WE SCAN THE CHARACTER ARRAY, ASSIGNING VARIABLES        

        if(base == 'R'){
            sscanf(C,"%i%c%i",&X,&base,&Z);
            Y = 0;
        }        
        else if(base == 'K'){
            X *= 1000;
            Y *= 100;
        }
        else if(base == 'M'){
            X *= 1000000;
            Y *= 100000;
        }

        integerTotal += getStockQuantity(S)*X;
        integerTotal += getStockQuantity(S)*Y;
        fractionTotal += getStockQuantity(S)*Z;
// THE CUMULATIVE TOTAL IS INCREMENTED AS APPROPRIATE.      
        
        if(fractionTotal >= 10){
            integerTotal += fractionTotal/10;
            fractionTotal = fractionTotal%10;
        }
    }
    
    printf("TOTAL RESISTANCE: %u.%i OHMS\n\n",integerTotal,fractionTotal);
// THE RESULT IS PRINTED LIKE SO.
}

// THE ANSWERS TO THE QUERIES ARE FOUND HERE.
int main(int argc, char** argv) {
    
// QUESTION 1 //////////////////////////////////////////////////////////////////    

    List* LP = loadInventory();       
    sortList(LP);
    printList(LP);
    
// QUESTION 2 //////////////////////////////////////////////////////////////////   
    
    List* SP = processTransactions(LP);    
    findSalesVolumeQuantity(SP);
    findSalesVolumePrice(SP);
    
    
// QUESTION 3 //////////////////////////////////////////////////////////////////   
    
    int x =  countTransistors(LP,"NPN");
    printf("\n\n%s%i\n\n","NUMBER OF NPN TRANSISTORS: ",x);
    
    
// QUESTION 4 //////////////////////////////////////////////////////////////////       
    
    findTotalResistance(LP);
 
    return (EXIT_SUCCESS);
}