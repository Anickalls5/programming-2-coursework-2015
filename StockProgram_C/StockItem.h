#ifndef STOCKITEM_H
#define STOCKITEM_H

// THIS FILE REPRESENTS STOCK ITEMS WITHIN THE PROGRAM AS STRUCTS.

typedef struct stockItemDT{
    char componentType[15];
    char stockCode[15];
    int stockQuantity;
    int unitPrice;   
    char componentInfo[20];
}
StockItem;

// ACCESSOR FUNCTIONS **********************************************************

// RETURNS THE COMPONENT TYPE
char* getComponentType(StockItem* S);

// RETURNS THE STOCK CODE
char* getStockCode(StockItem* S);

// RETURNS THE STOCK QUANTITY
int getStockQuantity(StockItem* S);

// RETURNS THE UNIT PRICE
int getUnitPrice(StockItem* S);

// RETURNS THE COMPONENT INFORMATION
char* getComponentInfo(StockItem* S);


// MUTATOR FUNCTIONS ***********************************************************

// SETS THE COMPONENT TYPE TO THE PASSED CHARACTER ARRAY
void setComponentType(StockItem* S, char* C);

// SETS THE STOCK CODE TO THE PASSED CHARACTER ARRAY
void setStockCode(StockItem* S, char* C);

// SETS THE STOCK QUANTITY TO THE PASSED INTEGER
void setStockQuantity(StockItem* S, int I);

// SETS THE UNIT PRICE TO THE PASSED INTEGER
void setUnitPrice(StockItem* S, int I);

// SETS THE COMPONENT INFORMATION TO THE PASSED CHARACTER ARRAY
char* setComponentInfo(StockItem* S, char* C);

#endif