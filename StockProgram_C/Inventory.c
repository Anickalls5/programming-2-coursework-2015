#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include "Inventory.h"

// THIS FUNCTION RETURNS AN INTEGER EQUIVALENT OF THE PASSED CHARACTER ARRAY, C.
// IT IS ASSUMED (AND ADVISED) THAT ONLY NUMBER STRINGS ARE PASSED AS ARGUMENTS.
int getInt(char* C){
    
    int length = strlen(C);
    int total = 0;
    int value;
    int base = 1;
    char digit;    
    
    for(int i = length-1; i >= 0; i--){
        digit = C[i];
        value = (int)digit - 48;  
        
        total += value*base;
        base *= 10;    
        
// ITERATING (IN REVERSE) THROUGH THE PASSED CHARACTER ARRAY, THE VALUE OF THE 
// CHARACTER IS OBTAINED BY SUBTRACTING THE DIFFERENCE BETWEEN DIGITS AND THEIR
// ASCII VALUES. THIS IS THEN ADDED TO A CUMULATIVE TOTAL.        
    }
    
    return total;
}


// LIST FUNCTIONS **************************************************************

// THIS FUNCTION RETURNS A LIST POINTER TO A DYNAMICALLY ALLOCATED BLOCK OF MEMORY.
List* createList(){
    
    List* LP = malloc(sizeof(Node));
// MEMORY IS DYNAMICALLY ALLOCATED BUT NOT INITIALISED.    
    
    if(LP == NULL){
        printf("%s%i","ERROR :: UNABLE TO ALLOCATE MEMORY FOR LIST OF SIZE ",getListLength(LP));
        return EXIT_FAILURE;
// IF THE MEMORY ALLOCATION HAS FAILED, THEN A NULL POINTER IS RETURNED. THE 
// PROGRAM THEN PRINTS AN ERROR MESSAGE AND TERMINATES.        
    }
    
    LP->head = NULL;
    LP->tail = NULL;    
    
    return LP;
}


// THIS FUNCTION READS THE inventory.txt FILE AND ADDS ALL OF THE STOCK 
// ITEMS TO A NEW LIST, THE POINTER OF WHICH IS RETURNED.
List* loadInventory(){
    
    List* LP = createList();
// A NEW LIST IS CREATED.    
    
    char buffer[200];
    
    FILE* FP;
    FP = fopen("inventory.txt","r");
// THE FILE IS OPENED FOR READING.
    
    while(fgets(buffer,200,FP) != NULL){
// THE fgets() FUNCTION READS THE LINE AND STORES IT IN THE BUFFER CHARACTER ARRAY.
// IF THE END OF THE FILE HAS BEEN REACHED, THEN THE WHILE LOOP WILL TERMINATE.        
        
        StockItem* SP = malloc(sizeof(StockItem));
// A NEW STOCK ITEM IS CREATED DYNAMICALLY.        
        
        char* componentType = strtok(buffer,", ");
        char* stockCode = strtok(NULL,", ");
        char* stockQuantity = strtok(NULL,", ");
        char* unitPrice = strtok(NULL,", ");
        char* componentInfo = strtok(NULL,", ");
// THE BUFFER CHARACTER ARRAY IS SPLIT INTO TOKENS, WHICH ARE THEN ASSIGNED 
// ACCORDINGLY.        
        
        setComponentType(SP,componentType);
        setStockCode(SP,stockCode);
        setStockQuantity(SP,getInt(stockQuantity));
        setUnitPrice(SP,getInt(unitPrice));

        if(strcmp(componentType,"diode") == 0){
            char* U = strtok(unitPrice,"\r");
            setUnitPrice(SP,getInt(U));
// BECAUSE THE LINES THAT SPECIFY DIODE COMPONENTS DO NOT HAVE ANY ADDITIONAL             
// INFORMATION, THE CARRIAGE RETRUN CHARACTER (\r) IS REMOVED AND THE UNIT PRICE
// ASSIGNED.            
        }
        else{       
            char* C = strtok(componentInfo,"\r");
            setComponentInfo(SP,C);
// IF THE COMPONENT IS NOT A DIODE, THE CARRIAGE RETURN CHARACTER IS REMOVED FROM
// THE COMPONENT INFORMATION INSTEAD.            
        }
        
        addStockItem(LP,SP);
// THE STOCK ITEM IS ADDED TO THE LIST.        
    }
    
    return LP;
// THE COMPLETED LIST IS RETURNED.    
}


// THIS FUNCTION WILL ITERATE THROUGH THE PASSED LIST AND WILL RETURN THE STOCK
// ITEM WHICH HAS THE PASSED STOCK CODE. IF THE STOCK ITEM IS NOT PRESENT, THEN
// NULL IS RETURNED.
StockItem* searchInventory(List* L, char* C){
    
    for(Node* N = L->head; N != NULL; N = N->next){
        StockItem* S = returnElement(N);
// ITERATING THROUGH THE GIVEN LIST, IF THE STOCK CODE OF THE CURRENT NODE
// MATCHES THE PASSED CHARACTER ARRAY, RETURN THE CURRENT NODE'S STOCK ITEM 
// ELEMENT.        
        
        if(strcmp(getStockCode(S),C) == 0){
            return S;
        }
    }
    
    return NULL;
// ELSE, NULL IS RETURNED.    
}


// THIS FUNCTION ADDS THE PASSED STOCK ITEM TO THE END OF THE PASSED LIST.
void addStockItem(List* L, StockItem* S){
    
    Node* NP = createNode();
    assignStockNode(NP,S);
// A NEW NODE IS CREATED AND THE STOCK ITEM IS ASSIGNED AS ITS ELEMENT.    
    
    if(L->head == NULL && L->tail == NULL){
        L->head = NP;
        L->tail = NP;
// IF THE LIST IS EMPTY, THE NEW NODE SHOULD BE SET AS BOTH THE HEAD AND TAIL OF 
// THE LIST.        
    }
    else{
        L->tail->next = NP;
        L->tail = NP;
// OTHERWISE, THE NODE SHOULD BE SET AS THE TAIL'S NEXT POINTER, AND THEN AS THE
// LIST'S NEW TAIL.        
    }
}


// THIS FUNCTION ADDS THE PASSED SALE TO THE END OF THE PASSED LIST.
void addSaleItem(List* L, Sale* S){
    
    Node* NP = createNode();
    assignSaleNode(NP,S);
// THE NODE IS CREATED AND THE SALE ITEM IS ASSIGNED TO IT.    
    
    if(L->head == NULL && L->tail == NULL){
        L->head = NP;
        L->tail = NP;
// IF THE LIST IS EMPTY, THEN THE NEW NODE IS SET AS BOTH THE LIST'S HEAD AND 
// TAIL.        
    }
    else{
        L->tail->next = NP;
        L->tail = NP;
// OTHERWISE, THE SALE BECOMES THE NEW TAIL OF THE LIST.    
    }
}


// THIS FUNCTION INSERTS THE PASSED STOCK ITEM AT THE HEAD OF THE PASSED LIST. 
void insertStockItem(List* L, StockItem* S){
    
    Node* NP = createNode();
    assignStockNode(NP,S);
// THE NODE IS CREATED AND THE STOCK ITEM ASSIGNED TO IT.    
    
    if(L->head == NULL && L->tail == NULL){
        L->head = NP;
        L->tail = NP;
// IF THE LIST IS EMPTY, THE NEW NODE IS SET AS THE LIST'S HEAD AND TAIL.        
    }
    else{
        L->head->next = L->head;
        L->head = NP;
// OTHERWISE, THE NEW NODE IS SET AS THE LIST'S NEW HEAD.        
    }
}


// THIS FUNCTIONS INSERTS THE PASSED SALE AT THE HEAD OF THE PASSED LIST.
void insertSaleItem(List* L, StockItem* S){
    
    Node* NP = createNode();
    assignSaleNode(NP,S);
// THE NODE IS CREATED AND THE SALE ASSIGNED TO IT.    
    
    if(L->head == NULL && L->tail == NULL){
        L->head = NP;
        L->tail = NP;
// IF THE LIST IS EMPTY, THE NEW NODE IS SET AS BOTH THE HEAD AND TAIL OF IT.        
    }
    else{
        L->head->next = L->head;
        L->head = NP;
// THE NEW NODE IS SET AS THE NEW HEAD OF THE LIST.        
    }
}


// THIS FUNCTION RETURNS AN INTEGER REPRESENTING THE NUMBER OF ITEMS WITHIN THE
// PASSED LIST.
int getListLength(List* L){
    
    int length = 0;    
    Node* N = L->head;
    
    for(N; N != NULL; N = N->next){
        length++;
// WE ITERATE THROUGH THE LIST AND INCREMENT THE LENGTH COUNT.        
    }
    
    return length;
}


// THIS FUNCTION DELETES THE PASSED LIST AND CREATES A NEW ONE IN ITS PLACE.
List* clearList(List* L){
    
    free(L);
// THE PASSED LIST IS DELETED FROM MEMORY.    
    
    List* LP = createList();
// THE NEW LIST IS CREATED AND RETURNED IN ITS PLACE.    
    
    return LP;
}


// THIS FUNCTION SORTS THE PASSED LIST INTO ITS NATURAL ORDER.
void sortList(List* L){
    
    int length = getListLength(L);
    int swapCount = 0, result = 0;
    
// IF THE LIST WAS EMPTY OR CONTAINS ONE NODE, THEN IT IS AUTOMATICALLY SORTED
// AND SO NOTHING IS DONE.    
    if(!(length <= 1)){
        do{            
            swapCount = 0;
            
            for(Node* N = L->head; N != NULL; N = N->next){
                
                Node* N1 = N;
                Node* N2 = N->next;
                
                if(N2 == NULL){
                    break;
                }

                result = compareNodes(N1,N2);
                
                if(result == 1){
                    swapNodes(L,N1,N2);
                    swapCount++;
                }
                
// OTHERWISE, WE ITERATE THROUGH THE LIST AND COMPARE TWO NODES. THE RESULT CHECK
// CAN BE INTERCHANGED TO ALTER THE DIRECTION OF THE LIST, FOR INSTANCE, 1 
// SORTS THE LIST INTO AN ASCENDING ORDER, WHILST -1 SORTS IT INTO A DESCENDING
// ORDER.                
            }
            
            if(swapCount == 0){
                break;
// IF NO SWAPS ARE MADE DURING AN ITERATION, THE LIST IS SORTED. WE BREAK OUT OF
// THE LOOP.                
            }
            
// WE LOOP CONTINUOUSLY UNTIL THE LIST IS SORTED AND THE LOOP IS BROKEN.                        
        }while(true);
    }
}


// THIS FUNCTION PRINTS THE PASSED LIST.
void printList(List* L){
    
    for(Node* N = L->head; N != NULL; N = N->next){
        printNode(N);
// ITERATE THROUGH THE PASSED LIST AND PRINT EACH NODE.        
    }
}


// NODE FUNCTIONS **************************************************************

// THIS FUNCTION CREATES A NEW NODE THROUGH DYNAMIC MEMORY ALLOCATION.
Node* createNode(){
    
    Node* NP = malloc(sizeof(Node));
    
    if(NP == NULL){
        printf("\n\n%s\n\n","ERROR :: UNABLE TO ALLOCATE MEMORY FOR NODE.");
        return EXIT_FAILURE;
// IF THE MEMORY CANNOT BE ALLOCATED, THE PROGRAM TERMINATES.        
    }
    
    NP->next = NULL;
    
    return NP;
}


// THIS FUNCTION SETS THE ELEMENT OF THE PASSED NODE TO THE PASSED STOCK ITEM.
void assignStockNode(Node* N, StockItem* S){    
    N->element.stockElement = S;
    N->nodeType = 0;
// THIS NODE TYPE IS USED TO DISTINGUISH A STOCKNODE FROM A SALENODE.    
}


// THIS FUNCTION SETS THE ELEMENT OF THE PASSED NODE TO THE PASSED SALE.
void assignSaleNode(Node* N, Sale* S){
    N->element.saleElement = S;
    N->nodeType = 1;
// THE ELEMENT AND NODE TYPE ARE SET.    
}


// THIS FUNCTION RETURNS THE ELEMENT ASSOCIATED WITH THE NODE.
void* returnElement(Node* N){
    
    if(N->nodeType == 0){
        return N->element.stockElement;
    }
    else{
        return N->element.saleElement;
    }
    
// BECAUSE THE ELEMENT OF A NODE IS A UNION, IT CAN ONLY EVER BE A STOCKNODE OR
// A SALENODE. THE NODE TYPES HELP TO DISTINGUISH WHICH TYPE OF ELEMENT TO RETURN.    
}


// THIS FUNCTION RETURNS THE IMMEDIATE PREDECESSOR OF THE PASSED NODE IN THE 
// PASSED LIST.
Node* getPrevious(List* L, Node* NP){
    
    for(Node* N = L->head; N != NULL; N = N->next){
        if(N->next == NP){
            return N;
        }
// WE ITERATE THROUGH THE PASSED LIST UNTIL WE FIND A NODE WHICH HAS A NEXT 
// POINTER POINTING AT THE PASSED NODE, WHERE IT IS THEN RETURNED.       
    }
}


// THIS FUNCTION SWAPS THE TWO PASSED NODES WITHIN THE PASSED LIST.
void swapNodes(List* L, Node* A, Node* B){

    if(A->nodeType == B->nodeType){
        
        StockItem* SA = returnElement(A);
        StockItem* SB = returnElement(B);
        
        assignStockNode(A,SB);
        assignStockNode(B,SA);
        
// REASSIGNING THE ELEMENTS OF THE TWO NODES GIVES THE SAME RESULT AS SWAPPING
// THEIR POSITIONS IN MEMORY.        
    }
}


// THIS FUNCTION COMPARES THE TWO PASSED NODES AND RETURNS AN INTEGER THAT 
// REFLECTS THE RELATIONSHIP: -1 IF NODE A IS SMALLER THAN NODE B, 1 IF NODE A 
// IS LARGER THAN NODE B AND 0 IF THE TWO ARE EQUAL.
int compareNodes(Node* A, Node* B){
        
    if(A->nodeType == B->nodeType){    
// THE NODES MUST BE OF THE SAME TYPE TO BE COMPARED. STOCK ITEMS ARE COMPARED 
// IN TERMS OF UNIT PRICE, WHILST SALES ARE COMPARED IN TERMS OF PROFIT
// (UNIT PRICE * QUANTITY SOLD).        
        
        if(A->nodeType == 0){            
            StockItem* SA = returnElement(A);
            StockItem* SB = returnElement(B);       
            
            if(SA->unitPrice > SB->unitPrice){
                return 1;
            }
            else if(SA->unitPrice < SB->unitPrice){
                return -1;
            }
            else{
                return 0;
            }
        }
        else{
            Sale* SA = returnElement(A);
            Sale* SB = returnElement(B);
            
            StockItem* SAI = getItemSold(SA);
            StockItem* SBI = getItemSold(SB);
            
            int profitA = getStockQuantity(SA)*getUnitPrice(SAI);
            int profitB = getStockQuantity(SB)*getUnitPrice(SBI);
            
            if(profitA > profitB){
                return 1;
            }
            else if(profitA < profitB){
                return -1;
            }
            else{
                return 0;
            }
        }   
    }   
}


// THIS FUNCTION PRINTS OUT THE PASSED NODE.
void printNode(Node* N){
        
    if(N->nodeType == 0){        
        StockItem* S = returnElement(N);
        if(strcmp(getComponentType(S),"diode") == 0){
            printf("STOCK ITEM: %-15s COMPONENT TYPE: %-15s QUANTITY: %-15i PRICE: %-16i\n",getStockCode(S),getComponentType(S),getStockQuantity(S),getUnitPrice(S));
        }
        else{
            printf("STOCK ITEM: %-15s COMPONENT TYPE: %-15s QUANTITY: %-15i PRICE: %-16i COMPONENT INFO: %-15s\n",getStockCode(S),getComponentType(S),getStockQuantity(S),getUnitPrice(S),getComponentInfo(S));
        }
    }
    else{
        Sale* S = returnElement(N);
        StockItem* SI = getItemSold(S);
        printf("ITEM SOLD: %-15s DATE: %-15s QUANTITY: %-15i\n",getStockCode(SI),getDate(S),getQuantitySold(S));
    }
}