#ifndef INVENTORY_H
#define INVENTORY_H
#include "StockItem.h"
#include "Sales.h"

// THIS FILE ACTS AS BOTH THE IMPLEMENTATION OF A LINKED LIST DATA STRUCTURE AND
// THE REPRESENTATION OF THE INVENTORY (A LINKED LIST OF STOCK ITEMS).

typedef struct nodeDT{
    
    union element{
        StockItem* stockElement;
        Sale* saleElement;
    }
    element;
    
    struct nodeDT* next;
    int nodeType;
    
// A ZERO REPRESENTS A STOCK ITEM NODE, WHILST A ONE REPRESENTS A SALE NODE.    
}
Node;

typedef struct listDT{
    Node* head;
    Node* tail;
}
List;


// RETURNS AN INTEGER EQUIVALENT OF THE PASSED CHARACTER ARRAY, C
int getInt(char* C);


// LIST FUNCTIONS **************************************************************

// RETURNS A LIST POINTER TO DYNAMICALLY ALLOCATED MEMORY
List* createList();

// READS THE inventory.txt FILE AND RETURNS A LIST OF THE STOCK ITEMS WITHIN IT
List* loadInventory();

// RETURNS THE STOCK ITEM WITH THE PASSED STOCK CODE WITHIN THE PASSED LIST
StockItem* searchInventory(List* L, char* C);

// ADDS THE PASSED STOCK ITEM TO THE END OF THE PASSED LIST
void addStockItem(List* L, StockItem* S);

// ADDS THE PASSED SALE TO THE END OF THE PASSED LIST
void addSaleItem(List* L, Sale* S);

// ADDS THE PASSED STOCK ITEM TO THE HEAD OF THE PASSED LIST
void insertStockItem(List* L, StockItem* S);

// ADDS THE PASSED SALE TO THE HEAD OF THE PASSED LIST
void insertSaleItem(List* L, StockItem* S);

// RETURNS THE NUMBER OF ITEMS WITHIN THE PASSED LIST
int getListLength(List* L);

// EMPTIES THE PASSED LIST
List* clearList(List* L);

// SORTS THE PASSED LIST INTO ITS NATURAL ORDERING
void sortList(List* L);

// PRINTS THE PASSED LIST
void printList(List* L);


// NODE FUNCTIONS **************************************************************

// RETURNS A NODE POINTER TO DYNAMICALLY ALLOCATED MEMORY
Node* createNode();

// SETS THE ELEMENT OF THE NODE POINTER TO THE PASSED STOCK ITEM 
void assignStockNode(Node* N, StockItem* S);

// SETS THE ELEMENT OF THE NODE POINTER TO THE PASSED SALE
void assignSaleNode(Node* N, Sale* S);

// RETURNS THE ELEMENT OF THE PASSED NODE POINTER
void* returnElement(Node* N);

// RETURNS THE NODE THAT HAS A NEXT POINTER SET TO THE PASSED NODE
Node* getPrevious(List* L, Node* NP);

// SWAPS THE ELEMENTS OF THE PASSED NODES WITHIN THE PASSED LIST
void swapNodes(List* L, Node* A, Node* B);

// COMPARES THE TWO PASSED NODE POINTERS
int compareNodes(Node* A, Node* B);

// PRINTS THE PASSED NODE
void printNode(Node* N);

#endif 