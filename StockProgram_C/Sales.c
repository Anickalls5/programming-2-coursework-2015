#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Sales.h"

// ACCESSOR FUNCTIONS **********************************************************

// THIS FUNCTION RETURNS THE STOCK ITEM POINTER THAT REPRESENTS THE ITEM SOLD
// WITHIN THE PASSED SALE POINTER.
StockItem* getItemSold(Sale* S){
    return S->itemSold;
}


// THIS FUNCTION RETURNS THE DATE OF THE PASSED SALE POINTER.
char* getDate(Sale* S){
    return S->date;
}


// THIS FUNCTION RETURNS THE INTEGER VALUE OF THE DAY WITHIN THE PASSED DATE 
// CHARACTER ARRAY, C. IT MUST BE NOTED THAT THE DATE PASSED IS IN THE FORM:
// DD/MM/YYYY.
int getDay(char* C){
    
    char* str = C;
    
    int digit1 = ((int)str[0] - 48)*10;
    int digit2 = (int)str[1] - 48;
// THERE IS A DIFFERENCE OF 48 BETWEEN A DIGIT AND ITS ASCII CODE (0 = 48...),
// HENCE, SUBTRACTING 48 FROM THE CAST WILL GIVE US THE INTEGER VALUE.
    
    return digit1+digit2;
}


// THIS FUNCTION RETURNS THE INTEGER VALUE OF THE MONTH WITHIN THE PASSED DATE 
// CHARACTER ARRAY, C. IT MUST BE NOTED THAT THE DATE PASSED IS IN THE FORM:
// DD/MM/YYYY.
int getMonth(char* C){
    
    char* str = C;
    
    int digit3 = ((int)str[3] - 48)*10;
    int digit4 = (int)str[4] - 48;
// THERE IS A DIFFERENCE OF 48 BETWEEN A DIGIT AND ITS ASCII CODE (0 = 48...),
// HENCE, SUBTRACTING 48 FROM THE CAST WILL GIVE US THE INTEGER VALUE.    
    
    return digit3+digit4;
}


// THIS FUNCTION RETURNS THE INTEGER VALUE OF THE YEAR WITHIN THE PASSED DATE 
// CHARACTER ARRAY, C. IT MUST BE NOTED THAT THE DATE PASSED IS IN THE FORM:
// DD/MM/YYYY.
int getYear(char* C){
    
    char* str = C;
    
    int digit5 = ((int)str[6] - 48)*1000;
    int digit6 = ((int)str[7] - 48)*100;
    int digit7 = ((int)str[8] - 48)*10;
    int digit8 = (int)str[9] - 48;
// THERE IS A DIFFERENCE OF 48 BETWEEN A DIGIT AND ITS ASCII CODE (0 = 48...),
// HENCE, SUBTRACTING 48 FROM THE CAST WILL GIVE US THE INTEGER VALUE.    
    
    return digit5+digit6+digit7+digit8;
}


// USING A VERSION OF ZELLER'S ALGORITHM, AN INTEGER REPRESENTING THE WEEKDAY OF
// THE PASSED DATE CHARACTER ARRAY IS RETURNED (SUNDAY = 0). AGAIN, IT MUST BE 
// NOTED THAT THE DATE PASSED IS IN THE FORM: DD/MM/YYYY.
int getWeekday(char* CH){
    
// THIS FUNCTION USES AN IMPLEMENTATION OF ZELLER'S ALGORITHM MENTIONED WITHIN
// THE DECISION MATHEMATICS TEXTBOOK FROM THE MEI MATHEMATICS COLLECTION (3RD ED).    

    int D = getDay(CH);
    int M = getMonth(CH);
    int Y = getYear(CH);
    
    if(M == 1 || M == 2){
        M += 12;
        Y--;
    }
    
    int FY = Y/100;
    int BY = Y%100;
    
    float A = (2.6*M) - 5.39;
    int B = (int)A + (int)(BY/4) + (int)(FY/4) + D + BY;
    int C = B - (2*FY);
    
    int R = C%7;
    
    return R;
}


// THE QUANTITY SOLD OF THE PASSED SALE POINTER IS RETURNED.
int getQuantitySold(Sale* S){
    return S->quantity;
}


// MUTATOR FUNCTIONS ***********************************************************

// THIS FUNCTION SETS THE ITEM SOLD OF THE PASSED SALE POINTER TO THE PASSED
// STOCK ITEM POINTER.
void setItemSold(Sale* S, StockItem* SI){
    S->itemSold = SI;
}


// THIS FUNCTION SETS THE DATE OF THE PASSED SALE POINTER TO THE PASSED CHARACTER
// ARRAY. IT MUST BE NOTED THAT THE DATE STRING SHOULD BE IN THE FORM: DD/MM/YYYY.
void setDate(Sale* S, char* C){
    
    int D = getDay(C);
    int M = getMonth(C);
    int Y = getYear(C);
    
    if(D < 1 || D > 31 || M < 1 || M > 12 || Y < 1980){       
        switch(M){
            case 2: if(D > 28 && Y%4 != 0){printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);} break;
            case 4: if(D > 30){printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);} break;
            case 6: if(D > 30){printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);} break;
            case 9: if(D > 30){printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);} break;
            case 11: if(D > 30){printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);} break;
            default : printf("\n\n%s\t(%i%i/%i%i/%i%i%i%i)","PLEASE INSERT A VALID DATE",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);        
        }
// THIS BLOCK PRINTS AN ERROR MESSAGE IF THE DATE PASSED IS NOT VALID. THE YEAR
// CONSTRAINT CAN BE ADJUSTED AS APPROPRIATE (I.E. Y < 2012...).        
    }
    else{    
        char str[11];

        sprintf(str,"%i%i/%i%i/%i%i%i%i",D/10,D%10,M/10,M%10,Y/1000,(Y/100)%10,(Y/10)%10,Y%10);

        strcpy(S->date,str);
// IF IT IS VALID, THEN IT IS WRITTEN TO THE SALE POINTER.    
    }
}


// THIS FUNCTION SETS THE QUANTITY SOLD OF THE PASSED SALE POINTER TO THE PASSED
// INTEGER, Q.
void setQuantitySold(Sale* S, int Q){
    S->quantity = Q;
}